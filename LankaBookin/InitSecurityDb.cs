﻿using LankaBookin.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;

namespace LankaBookin
{
    public class InitSecurityDb : DropCreateDatabaseIfModelChanges<LankaBookinEntities>
    {
        protected override void Seed(LankaBookinEntities context)
        {
            WebSecurity.InitializeDatabaseConnection("LankaBookinEntities", "Lb_User", "Id", "UserName", autoCreateTables: true);
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (!roles.RoleExists(Common.UserRoles.SuperAdmin.ToString()))
            {
                roles.CreateRole(Common.UserRoles.SuperAdmin.ToString());
            }

            IDictionary<string, object> values = new Dictionary<string, object>();

            values.Add("Name","Super Admin");
            values.Add("Address", "test");
            values.Add("Phone", "0716906809");
            values.Add("Email", "dhanulk87@gmail.com");

            if (membership.GetUser("LankaBookin", false) == null)
            {
                membership.CreateUserAndAccount("LankaBookin", "123456",values);
            }

            if (!roles.GetRolesForUser("LankaBookin").Contains("SuperAdmin"))
            {
                roles.AddUsersToRoles(new[] { "LankaBookin" }, new[] { "SuperAdmin" });
            }

            if (!roles.RoleExists(Common.UserRoles.Admin.ToString()))
            {
                roles.CreateRole(Common.UserRoles.Admin.ToString());
            }

            if (!roles.RoleExists(Common.UserRoles.Public.ToString()))
            {
                roles.CreateRole(Common.UserRoles.Public.ToString());
            }

            if (!roles.RoleExists(Common.UserRoles.Internal.ToString()))
            {
                roles.CreateRole(Common.UserRoles.Internal.ToString());
            }

            var baseDir = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin", string.Empty);

            context.Database.ExecuteSqlCommand(File.ReadAllText(baseDir + "\\InitialData.sql"));

            context.SaveChanges();

        }
    }
}