﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LankaBookin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "PropertySearch",
                url: "search/{term}",
                defaults: new { controller = "Apartment", action = "Search" }
            );

            routes.MapRoute(
                name: "PropertyDetail",
                url: "{PropertyName}/property_detail/{id}",
                defaults: new { controller = "Apartment", action = "Details", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ViewMap",
                url: "{PropertyName}/map/{id}",
                defaults: new { controller = "Apartment", action = "ViewMap", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}