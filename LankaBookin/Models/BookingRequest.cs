﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_BookingRequest")]
    public class BookingRequest:BaseModel
    {
        public BookingRequest()
        {
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "Apartment Name")]
        public Nullable<int> ApartmentId { get; set; }

        [Required]
        [Display(Name = "Price")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public decimal Price { get; set; }

        [Required]
        [Display(Name = "User Name")]
        public int UserId { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Order Date")]
        public System.DateTime OrderDate { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "CheckIn Date")]
        public System.DateTime CheckIn { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "CheckOut Date")]
        public System.DateTime CheckOut { get; set; }

        [Display(Name = "Number of rooms")]
        public int RoomsBooked { get; set; }

        public bool complete { get; set; }

        public virtual Apartment Apartment { get; set; }
        public virtual User User { get; set; }

    }
}