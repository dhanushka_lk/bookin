﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_TourRequest")]
    public class TourRequest:BaseModel
    {
        public TourRequest()
        {
            this.BookingRequests = new HashSet<BookingRequest>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int TourId { get; set; }
        public int Duration { get; set; }
        public Nullable<int> UserId { get; set; }
    
        public virtual Tour Lb_Tour { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<BookingRequest> BookingRequests { get; set; }
    }
}