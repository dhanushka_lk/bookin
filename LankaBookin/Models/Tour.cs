﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_Tour")]
    public class Tour:BaseModel
    {
        public Tour()
        {
            this.Categories = new HashSet<Category>();
            this.TourDestination = new HashSet<TourDestination>();
            this.TourRequest = new HashSet<TourRequest>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "Tour Name")]
        public string TourName { get; set; }

        [Display(Name = "Destination Points")]
        public int Steps { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        public string ImagePath { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<TourDestination> TourDestination { get; set; }
        public virtual ICollection<TourRequest> TourRequest { get; set; }
    }
}