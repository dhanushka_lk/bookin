﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_Category")]
    public class Category:BaseModel
    {
        public Category()
        {
            this.Tours = new HashSet<Tour>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }

        [Required]
        [Display(Name = "Description")]
        [MaxLength(200, ErrorMessage = "Description exceeds the maximum length")]
        public string Description { get; set; }

        public string ImagePath { get; set; }

        public virtual ICollection<Tour> Tours { get; set; }
    }
}