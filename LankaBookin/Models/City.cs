﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_City")]
    public class City:BaseModel
    {
        public City()
        {
            this.Apartment = new HashSet<Apartment>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "City Name")]
        public string CityName { get; set; }

        [Display(Name = "Is Top Destination")]
        public Nullable<bool> IsTopDestination { get; set; }

        [Display(Name = "Description")]
        [MaxLength(200, ErrorMessage = "Description exceeds the maximum length")]
        public string Description { get; set; }

        public int DistrictId { get; set; }

        public string ImagePath { get; set; }
    
        public virtual ICollection<Apartment> Apartment { get; set; }
        public virtual District District { get; set; }
    }
}