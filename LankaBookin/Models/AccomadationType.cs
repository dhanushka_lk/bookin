﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_AccomadationType")]
    public class AccomadationType : BaseModel
    {
        public AccomadationType()
        {
            this.Apartments = new HashSet<Apartment>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Accomadation Name")]
        [MaxLength(50, ErrorMessage = "Name exceeds the maximum length")]
        public string AccomadationName { get; set; }

        [Required]
        [Display(Name = "Description")]
        [MaxLength(200, ErrorMessage = "Description exceeds the maximum length")]
        public string Description { get; set; }

        public virtual ICollection<Apartment> Apartments { get; set; }
    }
}