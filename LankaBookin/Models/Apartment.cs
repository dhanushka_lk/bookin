﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_Apartment")]
    public class Apartment:BaseModel
    {
        public Apartment()
        {
            this.RoomsBook = new HashSet<RoomsBook>();
            this.Rating = new HashSet<Rating>();
            this.Reviews = new HashSet<Review>();
            this.UserWishList = new HashSet<UserWishList>();
            this.BookingRequest = new HashSet<BookingRequest>();
            this.Facilities = new HashSet<Facility>();
            this.Accomadations = new HashSet<AccomadationType>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Ref No")]
        public string RefNo { get; set; }

        [Required]
        [Display(Name = "Property Name")]
        [MaxLength(100, ErrorMessage = "Name exceeds the maximum length")]
        public string Name { get; set; }

        public decimal Rate { get; set; }

        [Required]
        [Display(Name = "Address Line 1")]
        [MaxLength(200, ErrorMessage = "Address exceeds the maximum length")]
        public string Address { get; set; }

        [Display(Name = "Address Line 2")]
        [MaxLength(200, ErrorMessage = "Address exceeds the maximum length")]
        public string Address2 { get; set; }

        [Required]
        [Display(Name = "Description")]
        [MaxLength(10000, ErrorMessage = "Description exceeds the maximum length")]
        public string Description { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "MetaDescription")]
        public string MetaDescription { get; set; }

        [Display(Name = "Per room basis")]
        public bool IsPerRoom { get; set; }

        [Display(Name = "Per room per night")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public Nullable<decimal> PricePerRoom { get; set; }

        [Display(Name = "Whole property per night")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public Nullable<decimal> WholePricePerDay { get; set; }

        [Display(Name = "Whole property per week")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public Nullable<decimal> WholePricePerWeek { get; set; }

        [Display(Name = "Whole property per month")]
        [DisplayFormat(DataFormatString = "{0:C}")]
        public Nullable<decimal> WholePricePerMonth { get; set; }

        [Display(Name = "Sleeps")]
        public int MaxHeadCount { get; set; }

        [Display(Name = "Bedrooms")]
        public int NoOfBedrooms { get; set; }

        [Display(Name = "Bathrooms")]
        public int NoOfBathrooms { get; set; }

        [Required]
        [Display(Name = "User Name")]
        public int UserId { get; set; }

        [Required]
        [Display(Name = "City Name")]
        public int CityId { get; set; }

        [Display(Name = "Lankmark Name")]
        public Nullable<int> LandmarkId { get; set; }

        public string Lat { get; set; }

        public string Lan { get; set; }

        public string MainImagePath { get; set; }
    
        public virtual City City { get; set; }
        public virtual User User { get; set; }
        public virtual Landmark Landmark { get; set; }
        public virtual ICollection<RoomsBook> RoomsBook { get; set; }
        public virtual ICollection<Rating> Rating { get; set; }
        public virtual ICollection<UserWishList> UserWishList { get; set; }
        public virtual ICollection<BookingRequest> BookingRequest { get; set; }
        public virtual ICollection<Facility> Facilities { get; set; }
        public virtual ICollection<AccomadationType> Accomadations { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
    }
}