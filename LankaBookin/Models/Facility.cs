﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_Facility")]
    public class Facility : BaseModel
    {
        public Facility()
        {
            this.Apartments = new HashSet<Apartment>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Amenity Name")]
        [MaxLength(100, ErrorMessage = "Name exceeds the maximum length")]
        public string FacilityName { get; set; }

        public int FacilityCategoryId { get; set; }

        public int Priority { get; set; }

        [Required]
        [Display(Name = "Description")]
        [MaxLength(200, ErrorMessage = "Description exceeds the maximum length")]
        public string Description { get; set; }

        public virtual ICollection<Apartment> Apartments { get; set; }

        public virtual FacilityCategory FacilityCategory { get; set; }
    }
}