﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_District")]
    public class District:BaseModel
    {
        public District()
        {
            this.City = new HashSet<City>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [Display(Name = "District Name")]
        public string DistrictName { get; set; }

        [Display(Name = "Description")]
        [MaxLength(200, ErrorMessage = "Description exceeds the maximum length")]
        public string Description { get; set; }
     
        public virtual ICollection<City> City { get; set; }
    }
}