﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LankaBookin.Models
{
    [Table("Lb_Rating")]
    public class Rating : BaseModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "Apartment Name")]
        public Nullable<int> ApartmentId { get; set; }

        [Display(Name = "User Name")]
        public Nullable<int> UserId { get; set; }

        [Required]
        [Display(Name = "Rate")]
        public int Rate { get; set; }

        public virtual Apartment Apartment { get; set; }
        public virtual User User { get; set; }
    }
}