﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using System.Web.Security;
using WebMatrix.WebData;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class UserController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /User/

        public ActionResult Index()
        {
            return View(db.User.Where(u=>u.Id!=1).ToList());
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(int id = 0)
        {
            User lb_user = db.User.Find(id);
            Role userRole = (from ur in db.UsersInRoles
                             join role in db.Roles on ur.RoleId equals role.RoleId
                             where ur.UserId == id
                             select role).SingleOrDefault();

            ViewBag.UserRole = userRole;

            if (lb_user == null)
            {
                return HttpNotFound();
            }
            return View(lb_user);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            var userRoles = db.Roles.Where(r=>r.RoleId!=1).ToList();
            ViewBag.UserRoles = new SelectList(userRoles, "RoleId", "RoleName");
            return View();
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(User user, int UserType)
        {
            if (ModelState.IsValid)
            {
                WebSecurity.CreateUserAndAccount(user.UserName, "123456", propertyValues: new
                {
                    Name = user.Name,
                    Address = user.Address,
                    Phone = user.Phone,
                    Email = user.Email,
                    Deleted=false,
                });

                var userRole = db.Roles.Find(UserType);

                if (userRole != null)
                {
                    Roles.AddUserToRole(user.UserName, userRole.RoleName);
                }

                return RedirectToAction("Index");
            }

            var userRoles = db.Roles.Where(r => r.RoleId != 1).ToList();
            ViewBag.UserRoles = new SelectList(userRoles, "RoleId", "RoleName");
            return View(user);
        }

        //
        // GET: /User/Edit/5
        [Authorize(Roles="SuperAdmin")]
        public ActionResult Edit(int id = 0)
        {
            User lb_user = db.User.Find(id);
            if (lb_user == null)
            {
                return HttpNotFound();
            }

            return View(lb_user);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Edit(User user)
        {
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            if (ModelState.IsValid)
            {
                user.ModifiedBy = userId;
                user.ModifiedDate = DateTime.Now;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(user);
        }

        //
        // GET: /User/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            User lb_user = db.User.Find(id);
            if (lb_user == null)
            {
                return HttpNotFound();
            }
            return View(lb_user);
        }

        //
        // POST: /User/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            User lb_user = db.User.Find(id);
            db.User.Remove(lb_user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}