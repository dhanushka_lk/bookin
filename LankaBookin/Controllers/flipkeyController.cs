﻿using LankaBookin.Common;
using LankaBookin.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin,Host")]
    public class flipkeyController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /flipkey/

        public ActionResult getproperties()
        {
            var apartments = db.Apartment.Where(a => a.Deleted != true);

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode root = doc.CreateElement("PropertySummary");
            doc.AppendChild(root);

            foreach (var item in apartments)
            {
                XmlNode Property = doc.CreateElement("Property");

                XmlAttribute PropertyAttribute = doc.CreateAttribute("property_id");
                PropertyAttribute.Value =Convert.ToString(item.Id);
                XmlAttribute PropertyAttribute1 = doc.CreateAttribute("last_update");

                var date = item.ModifiedDate;

                if (date == null)
                {
                    date = DateTime.Now;
                }
                else if (Convert.ToString(date) == string.Empty)
                {
                    date = DateTime.Now;
                }

                PropertyAttribute1.Value = Convert.ToString(date);

                Property.Attributes.Append(PropertyAttribute);
                Property.Attributes.Append(PropertyAttribute1);
                root.AppendChild(Property);
            }

            string returnText = string.Empty;

            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter))
            {
                doc.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                returnText=stringWriter.GetStringBuilder().ToString();
            }

            return this.Content(returnText, "text/xml");
        }

        public ActionResult getproperty(int property_id)
        {
            var property = db.Apartment.Find(property_id);
            if (property == null)
            {

            }

            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode Property = doc.CreateElement("Property");

            XmlAttribute PropertyAttribute = doc.CreateAttribute("property_id");
            PropertyAttribute.Value = Convert.ToString(property.Id);
            XmlAttribute PropertyAttribute1 = doc.CreateAttribute("last_update");

            var date = property.ModifiedDate;

            if (date == null)
            {
                date = DateTime.Now;
            }
            else if (Convert.ToString(date) == string.Empty)
            {
                date = DateTime.Now;
            }

            PropertyAttribute1.Value = Convert.ToString(date);

            Property.Attributes.Append(PropertyAttribute);
            Property.Attributes.Append(PropertyAttribute1);
            doc.AppendChild(Property);

            XmlNode nameNode = doc.CreateElement("PropertyName");
            nameNode.AppendChild(doc.CreateTextNode(property.Name));
            Property.AppendChild(nameNode);

            XmlNode address = doc.CreateElement("Address");

            XmlNode address1 = doc.CreateElement("Address1");
            address1.AppendChild(doc.CreateTextNode(property.Address));

            address.AppendChild(address1);

            XmlNode address2 = doc.CreateElement("Address2");
            address2.AppendChild(doc.CreateTextNode(property.Address2));

            address.AppendChild(address2);

            XmlNode city = doc.CreateElement("City");
            city.AppendChild(doc.CreateTextNode(property.City.CityName));

            address.AppendChild(city);

            var dictrict = db.City.Where(c => c.Id == property.CityId).FirstOrDefault().District.DistrictName;

            XmlNode state = doc.CreateElement("State");
            state.AppendChild(doc.CreateTextNode(dictrict));

            address.AppendChild(state);

            XmlNode country = doc.CreateElement("Country");
            country.AppendChild(doc.CreateTextNode("Sri Lanka"));

            address.AppendChild(country);

            XmlNode latitude = doc.CreateElement("Latitude");
            latitude.AppendChild(doc.CreateTextNode(property.Lat));

            address.AppendChild(latitude);

            XmlNode longitude = doc.CreateElement("Longitude");
            longitude.AppendChild(doc.CreateTextNode(property.Lan));

            address.AppendChild(longitude);

            Property.AppendChild(address);

            XmlNode details = doc.CreateElement("Details");

            XmlNode maximumOccupancy = doc.CreateElement("MaximumOccupancy");
            maximumOccupancy.AppendChild(doc.CreateTextNode(Convert.ToString(property.MaxHeadCount)));

            details.AppendChild(maximumOccupancy);

            XmlNode bedrooms = doc.CreateElement("Bedrooms");
            XmlAttribute bedcount = doc.CreateAttribute("count");
            bedcount.Value = Convert.ToString(property.NoOfBedrooms);
            bedrooms.Attributes.Append(bedcount);

            details.AppendChild(bedrooms);

            XmlNode bathrooms = doc.CreateElement("Bathrooms");
            XmlAttribute bathcount = doc.CreateAttribute("count");
            bathcount.Value = Convert.ToString(property.NoOfBathrooms);
            bathrooms.Attributes.Append(bathcount);

            details.AppendChild(bathrooms);

            Property.AppendChild(details);

            XmlNode descriptions = doc.CreateElement("Descriptions");

            XmlNode propertyDescription = doc.CreateElement("PropertyDescription");
            propertyDescription.AppendChild(doc.CreateTextNode(property.Description));

            descriptions.AppendChild(propertyDescription);

            Property.AppendChild(descriptions);

            XmlNode amenities = doc.CreateElement("Amenities");

            XmlNode amenity;
            XmlAttribute order;
            int flag = 1;

            foreach (var value in property.Facilities)
            {
                amenity = doc.CreateElement("Amenity");
                order = doc.CreateAttribute("order");
                order.Value = Convert.ToString(flag);
                amenity.Attributes.Append(order);
                amenity.AppendChild(doc.CreateTextNode(value.FacilityName));
                amenities.AppendChild(amenity);
                flag++;
            }

            Property.AppendChild(amenities);

            XmlNode photos = doc.CreateElement("Photos");
            XmlNode photo;
            XmlNode url;

            if (Directory.Exists(Server.MapPath("~/Images/Apartment/" + property.Name)))
            {
                foreach (var imgPath in Directory.GetFiles(Server.MapPath("~/Images/Apartment/" + property.Name)))
                {
                    var file = new FileInfo(imgPath);
                    var filename=string.Format("{0}/{1}/{2}","http://www.lankabookin.com/Images/Apartment",property.Name,file.Name);
                    photo = doc.CreateElement("Photo");
                    url = doc.CreateElement("URL");
                    url.AppendChild(doc.CreateTextNode(filename));
                    photo.AppendChild(url);
                    photos.AppendChild(photo);
                }
            }

            Property.AppendChild(photos);

            XmlNode rates = doc.CreateElement("Rates");

            XmlNode defaultRate = doc.CreateElement("DefaultRate");

            XmlNode dailyRate = doc.CreateElement("DailyRate");
            if (property.IsPerRoom)
            {
                dailyRate.AppendChild(doc.CreateTextNode(Convert.ToString(property.PricePerRoom)));
            }
            else
            {
                dailyRate.AppendChild(doc.CreateTextNode(Convert.ToString(property.WholePricePerDay)));
            }

            defaultRate.AppendChild(dailyRate);

            XmlNode weeklyRate = doc.CreateElement("WeeklyRate");
            weeklyRate.AppendChild(doc.CreateTextNode(Convert.ToString(property.WholePricePerWeek)));

            defaultRate.AppendChild(weeklyRate);

            XmlNode monthlyRate = doc.CreateElement("MonthlyRate");
            monthlyRate.AppendChild(doc.CreateTextNode(Convert.ToString(property.WholePricePerMonth)));

            defaultRate.AppendChild(monthlyRate);

            rates.AppendChild(defaultRate);
            Property.AppendChild(rates);

            string returnText = string.Empty;

            using (var stringWriter = new StringWriter())
            using (var xmlTextWriter = XmlWriter.Create(stringWriter))
            {
                doc.WriteTo(xmlTextWriter);
                xmlTextWriter.Flush();
                returnText = stringWriter.GetStringBuilder().ToString();
            }

            return this.Content(returnText, "text/xml");

        }

        public ActionResult getavailability(int property_id = 0)
        {
            string toFormat = "yyyy-MM-dd";
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode root = doc.CreateElement("Availability");
            doc.AppendChild(root);

            if (property_id == 0)
            {
                var apartments = db.Apartment.Where(a => a.Deleted != true).ToList();

                foreach (var apartment in apartments)
                {
                    XmlNode Property = doc.CreateElement("BookedStays");

                    XmlAttribute PropertyAttribute = doc.CreateAttribute("property_id");
                    PropertyAttribute.Value = Convert.ToString(apartment.Id);

                    Property.Attributes.Append(PropertyAttribute);

                    var result = db.RoomsBook.Where(r => r.ApartmentId == apartment.Id && r.booked == true).ToList();

                    foreach (var item in result)
                    {
                        XmlNode bookedStay = doc.CreateElement("BookedStay");

                        XmlNode arrivalDate = doc.CreateElement("ArrivalDate");
                        arrivalDate.AppendChild(doc.CreateTextNode(item.Date.ToString(toFormat)));

                        XmlNode departureDate = doc.CreateElement("DepartureDate");
                        departureDate.AppendChild(doc.CreateTextNode(item.Date.ToString(toFormat)));

                        bookedStay.AppendChild(arrivalDate);
                        bookedStay.AppendChild(departureDate);

                        Property.AppendChild(bookedStay);
                    }

                    root.AppendChild(Property);
                }

                string returnText = string.Empty;

                using (var stringWriter = new StringWriter())
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    doc.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    returnText = stringWriter.GetStringBuilder().ToString();
                }

                return this.Content(returnText, "text/xml");
            }
            else
            {
                var property = db.Apartment.Find(property_id);
                if (property == null)
                {

                }


                XmlNode Property = doc.CreateElement("BookedStays");

                XmlAttribute PropertyAttribute = doc.CreateAttribute("property_id");
                PropertyAttribute.Value = Convert.ToString(property_id);

                Property.Attributes.Append(PropertyAttribute);

                var result = db.RoomsBook.Where(r => r.ApartmentId == property_id && r.booked == true).ToList();

                foreach (var item in result)
                {
                    XmlNode bookedStay = doc.CreateElement("BookedStay");

                    XmlNode arrivalDate = doc.CreateElement("ArrivalDate");
                    arrivalDate.AppendChild(doc.CreateTextNode(item.Date.ToString(toFormat)));

                    XmlNode departureDate = doc.CreateElement("DepartureDate");
                    departureDate.AppendChild(doc.CreateTextNode(item.Date.ToString(toFormat)));

                    bookedStay.AppendChild(arrivalDate);
                    bookedStay.AppendChild(departureDate);

                    Property.AppendChild(bookedStay);
                }

                root.AppendChild(Property);

                string returnText = string.Empty;

                using (var stringWriter = new StringWriter())
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    doc.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    returnText = stringWriter.GetStringBuilder().ToString();
                }

                return this.Content(returnText, "text/xml");
            }
        }

    }
}
