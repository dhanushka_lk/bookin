﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using LankaBookin.Common;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class AdminBookingRequestController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /AdminBookingRequest/

        public ActionResult Index()
        {
            var bookingrequest = db.BookingRequest.Where(b => b.Deleted != true).ToList();
            return View(bookingrequest);
        }

        //
        // GET: /AdminBookingRequest/Details/5

        public ActionResult Details(int id = 0)
        {
            BookingRequest bookingrequest = db.BookingRequest.Find(id);
            if (bookingrequest == null)
            {
                return HttpNotFound();
            }
            return View(bookingrequest);
        }
       

        //
        // GET: /AdminBookingRequest/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    BookingRequest bookingrequest = db.BookingRequest.Find(id);
        //    if (bookingrequest == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ApartmentId = new SelectList(db.Apartment, "Id", "Name", bookingrequest.ApartmentId);
        //    ViewBag.UserId = new SelectList(db.User, "Id", "Name", bookingrequest.UserId);
        //    return View(bookingrequest);
        //}

        ////
        //// POST: /AdminBookingRequest/Edit/5

        //[HttpPost]
        //public ActionResult Edit(BookingRequest bookingrequest)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        int userId = Convert.ToInt32(Session[Common.SessionVariables.UserId.ToString()]);
        //        bookingrequest.ModifiedBy = userId;
        //        bookingrequest.ModifiedDate = DateTime.Now;
        //        db.Entry(bookingrequest).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ApartmentId = new SelectList(db.Apartment, "Id", "Name", bookingrequest.ApartmentId);
        //    ViewBag.UserId = new SelectList(db.User, "Id", "Name", bookingrequest.UserId);
        //    return View(bookingrequest);
        //}

        //
        // GET: /AdminBookingRequest/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            BookingRequest bookingrequest = db.BookingRequest.Find(id);
            if (bookingrequest == null)
            {
                return HttpNotFound();
            }
            return View(bookingrequest);
        }

        //
        // POST: /AdminBookingRequest/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            BookingRequest bookingrequest = db.BookingRequest.Find(id);
            bookingrequest.Deleted = true;
            db.Entry(bookingrequest).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult IsPerRoom(int apartmentId)
        {
            Apartment apartment = db.Apartment.Find(apartmentId);
            return Json(new { isPerRoom = apartment.IsPerRoom, price = apartment.PricePerRoom }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CalculatePrice(int apartmentId, int roomCount)
        {
            Apartment apartment = db.Apartment.Find(apartmentId);
            var price = apartment.PricePerRoom * roomCount;
            return Json(new { price = price }, JsonRequestBehavior.AllowGet);
        }
    }
}