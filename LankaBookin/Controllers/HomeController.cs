﻿using LankaBookin.DAL;
using LankaBookin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace LankaBookin.Controllers
{
    public class HomeController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        public ActionResult Index()
        {
            try
            {
                if (User.IsInRole(Common.UserRoles.Admin.ToString()) || User.IsInRole(Common.UserRoles.SuperAdmin.ToString()))
                {
                    return RedirectToAction("Index", "User");
                }
                else if (User.IsInRole(Common.UserRoles.Internal.ToString()))
                {
                    return RedirectToAction("Index", "Apartment");
                }

                ViewBag.districtId = new SelectList(db.District.OrderBy(d => d.DistrictName), "Id", "DistrictName");
                ViewBag.cityId = new SelectList(db.City.Where(c=>c.Id==1), "Id", "CityName");

                var apartments = db.Apartment.Where(a => a.Deleted != true).OrderByDescending(a => a.ModifiedDate).Take(6);

                ViewBag.topDestinations = db.City.Where(a => a.Deleted != true && a.IsTopDestination == true).Take(9);
                ViewBag.Category = db.Category.Where(c => c.Deleted != true).ToList();

                return View(apartments.ToList());
            }
            catch (Exception)
            {
                if (User.Identity.IsAuthenticated)
                {
                    WebSecurity.Logout();
                }

                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        [HttpPost]
        public ActionResult SaveData()
        {
            return RedirectToAction("Index");
        }

        public ActionResult TrustSafty()
        {
            return View();
        }

        public ActionResult LankabookinPics()
        {
            return View();
        }

        public ActionResult Mobile()
        {
            return View();
        }

        public ActionResult WhyHost()
        {
            return View();
        }

        public ActionResult Blog()
        {
            return View();
        }

        public ActionResult Policies()
        {
            return View();
        }

        public ActionResult ResponsibleHosting()
        {
            return View();
        }

        public ActionResult TermsPrivacy()
        {
            return View();
        }

        public ActionResult Health()
        {
            return View();
        }

        public ActionResult Vision()
        {
            return View();
        }

        public ActionResult Newtolankabookin()
        {
            return View();
        }
    }
}
