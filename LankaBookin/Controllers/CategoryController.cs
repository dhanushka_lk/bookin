﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using WebMatrix.WebData;
using System.IO;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class CategoryController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /Category/

        public ActionResult Index()
        {
            return View(db.Category.ToList());
        }

        //
        // GET: /Category/Details/5

        public ActionResult Details(int id = 0)
        {
            Category lb_category = db.Category.Find(id);
            if (lb_category == null)
            {
                return HttpNotFound();
            }

            return View(lb_category);
        }

        //
        // GET: /Category/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Category/Create

        [HttpPost]
        public ActionResult Create(Category lb_category, HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError("upload", "Please upload the image");
            }

            if (ModelState.IsValid)
            {

                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string path = Server.MapPath("~/Images/Category/");
                    string fullPath = System.IO.Path.Combine(path, pic);

                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    // file is uploaded
                    file.SaveAs(fullPath);
                    lb_category.ImagePath = fullPath;
                }

                int userId = WebSecurity.GetUserId(User.Identity.Name);
                lb_category.CreatedBy = userId;
                lb_category.CreatedDate = DateTime.Now;
                lb_category.Deleted = false;
                db.Category.Add(lb_category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lb_category);
        }

        //
        // GET: /Category/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Category lb_category = db.Category.Find(id);
            if (lb_category == null)
            {
                return HttpNotFound();
            }
            return View(lb_category);
        }

        //
        // POST: /Category/Edit/5

        [HttpPost]
        public ActionResult Edit(Category lb_category, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    if (System.IO.File.Exists(lb_category.ImagePath))
                    {
                        System.IO.File.Delete(lb_category.ImagePath);
                    }

                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string path = Server.MapPath("~/Images/Category/");
                    string fullPath = System.IO.Path.Combine(path, pic);

                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    // file is uploaded
                    file.SaveAs(fullPath);
                    lb_category.ImagePath = fullPath;
                }

                int userId = WebSecurity.GetUserId(User.Identity.Name);
                lb_category.ModifiedBy = userId;
                lb_category.ModifiedDate = DateTime.Now;
                db.Entry(lb_category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lb_category);
        }

        //
        // GET: /Category/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            Category lb_category = db.Category.Find(id);
            if (lb_category == null)
            {
                return HttpNotFound();
            }
            return View(lb_category);
        }

        //
        // POST: /Category/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Category lb_category = db.Category.Find(id);
            db.Category.Remove(lb_category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}