﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using WebMatrix.WebData;
using LankaBookin.Common;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class FacilityController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /Facility/

        public ActionResult Index()
        {
            return View(db.Facility.ToList());
        }

        //
        // GET: /Facility/Create

        public ActionResult Create()
        {
            var myListItems = new List<SelectListItem>();
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.High).ToString(),
                Text = Priority.High.ToString()
            });
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.Normal).ToString(),
                Text = Priority.Normal.ToString()
            });
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.Low).ToString(),
                Text = Priority.Low.ToString()
            });

            ViewBag.Priority = new SelectList(myListItems, "Value", "Text");

            ViewBag.FacilityCategoryId = new SelectList(db.FacilityCategory.Where(C => C.Deleted != true), "Id", "FacilityCategoryName");
            return View();
        }

        //
        // POST: /Facility/Create

        [HttpPost]
        public ActionResult Create(Facility facility)
        {
            if (ModelState.IsValid)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                facility.CreatedBy = userId;
                facility.CreatedDate = DateTime.Now;
                facility.Deleted = false;
                db.Facility.Add(facility);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var myListItems = new List<SelectListItem>();
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.High).ToString(),
                Text = Priority.High.ToString()
            });
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.Normal).ToString(),
                Text = Priority.Normal.ToString()
            });
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.Low).ToString(),
                Text = Priority.Low.ToString()
            });

            ViewBag.Priority = new SelectList(myListItems, "Value", "Text");

            ViewBag.FacilityCategoryId = new SelectList(db.Facility.Where(C => C.Deleted != true), "Id", "FacilityCategoryName");
            return View(facility);
        }

        //
        // GET: /Facility/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Facility facility = db.Facility.Find(id);
            if (facility == null)
            {
                return HttpNotFound();
            }

            var myListItems = new List<SelectListItem>();
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.High).ToString(),
                Text = Priority.High.ToString()
            });
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.Normal).ToString(),
                Text = Priority.Normal.ToString()
            });
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.Low).ToString(),
                Text = Priority.Low.ToString()
            });

            ViewBag.Priority = new SelectList(myListItems, "Value", "Text",facility.Priority);

            ViewBag.FacilityCategoryId = new SelectList(db.FacilityCategory.Where(C => C.Deleted != true), "Id", "FacilityCategoryName",facility.FacilityCategoryId);
            return View(facility);
        }

        //
        // POST: /Facility/Edit/5

        [HttpPost]
        public ActionResult Edit(Facility facility)
        {
            if (ModelState.IsValid)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                facility.ModifiedBy = userId;
                facility.ModifiedDate = DateTime.Now;
                db.Entry(facility).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var myListItems = new List<SelectListItem>();
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.High).ToString(),
                Text = Priority.High.ToString()
            });
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.Normal).ToString(),
                Text = Priority.Normal.ToString()
            });
            myListItems.Add(new SelectListItem
            {
                Value = ((int)Priority.Low).ToString(),
                Text = Priority.Low.ToString()
            });

            ViewBag.Priority = new SelectList(myListItems, "Value", "Text", facility.Priority);

            ViewBag.FacilityCategoryId = new SelectList(db.FacilityCategory.Where(C => C.Deleted != true), "Id", "FacilityCategoryName",facility.FacilityCategoryId);
            return View(facility);
        }

        //
        // GET: /Facility/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            Facility lb_facility = db.Facility.Find(id);
            if (lb_facility == null)
            {
                return HttpNotFound();
            }
            return View(lb_facility);
        }

        //
        // POST: /Facility/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Facility lb_facility = db.Facility.Find(id);
            db.Facility.Remove(lb_facility);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}