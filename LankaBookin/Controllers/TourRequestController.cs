﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using WebMatrix.WebData;
using System.Net.Mail;

namespace LankaBookin.Controllers
{
    [Authorize]
    public class TourRequestController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /TourRequest/
        [Authorize(Roles = "Public")]
        public ActionResult Index(bool display = false)
        {
            ViewBag.Display = display;
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            var tourRequest = db.TourRequest.Where(b => b.UserId == userId).ToList();
            return View(tourRequest.ToList());
        }

        //
        // GET: /TourRequest/Details/5

        public ActionResult Details(int id = 0)
        {
            TourRequest lb_tourrequest = db.TourRequest.Find(id);
            if (lb_tourrequest == null)
            {
                return HttpNotFound();
            }
            return View(lb_tourrequest);
        }

        //
        // GET: /TourRequest/Create
        [Authorize(Roles="Public")]
        public ActionResult Create(int tourId=0)
        {
            Tour lb_tour = null;
            HttpCookie cookie = null;

            if (HttpContext.Request.Cookies["TourRequest"] == null)
            {
                lb_tour = db.Tour.Find(tourId);
                if (lb_tour == null)
                {
                    return HttpNotFound();
                }

                cookie = new HttpCookie("TourRequest");
                cookie["tourId"] = tourId.ToString();
                HttpContext.Response.Cookies.Remove("TourRequest");
                HttpContext.Response.SetCookie(cookie);
            }
            else
            {
                cookie = HttpContext.Request.Cookies.Get("TourRequest");
                var id = cookie["tourId"];
                if (!string.IsNullOrEmpty(id))
                {
                    tourId = Convert.ToInt32(id);
                    lb_tour = db.Tour.Find(tourId);
                    if (lb_tour == null)
                    {
                        return HttpNotFound();
                    }
                }

                if (cookie != null)
                {
                    Dictionary<int, object> dic = new Dictionary<int, object>();
                    foreach (var item in lb_tour.TourDestination)
                    {
                        var bookinRequestId = cookie[item.Id.ToString()];
                        if (!string.IsNullOrEmpty(bookinRequestId))
                        {
                            var bookinRequest = db.BookingRequest.Find(Convert.ToInt32(bookinRequestId));
                            if (bookinRequest != null)
                            {
                                dic.Add(item.Id, bookinRequest);
                            }
                        }
                    }
                    ViewBag.BookinRequests = dic;
                }
            }

            ViewBag.tourId = tourId;
            ViewBag.Tour = lb_tour;
            ViewBag.CityList = new SelectList(db.City, "CityId", "City");

            return View();
        }

        //
        // POST: /TourRequest/Create

        [HttpPost]
        [Authorize(Roles = "Public")]
        public ActionResult Create(TourRequest lb_tourrequest, int tourId)
        {
            if (ModelState.IsValid)
            {
                HttpCookie cookie = null;

                if (HttpContext.Request.Cookies["TourRequest"] != null)
                {
                    int userId = WebSecurity.GetUserId(User.Identity.Name);
                    cookie = HttpContext.Request.Cookies.Get("TourRequest");

                    if (cookie != null)
                    {
                        Tour tour = null;
                        if (tourId>0)
                        {
                            tour = db.Tour.Find(tourId);
                        }

                        TourRequest tourRequest = new TourRequest
                        {
                            Deleted = false,
                            TourId = tourId,
                            Duration = lb_tourrequest.Duration,
                            UserId = userId,
                            CreatedBy = userId,
                            CreatedDate = DateTime.Now
                        };


                        foreach (var item in tour.TourDestination)
                        {
                            var bookinRequestId = cookie[item.Id.ToString()];
                            if (!string.IsNullOrEmpty(bookinRequestId))
                            {
                                var bookinRequest = db.BookingRequest.Find(Convert.ToInt32(bookinRequestId));
                                if (bookinRequest != null)
                                {
                                    bookinRequest.complete = true;
                                    db.SaveChanges();
                                    tourRequest.BookingRequests.Add(bookinRequest);

                                }
                            }
                        }

                        db.TourRequest.Add(tourRequest);
                        db.SaveChanges();

                        cookie.Expires = DateTime.Now.AddDays(-1);
                        HttpContext.Response.SetCookie(cookie);
                    }

                }

                try
                {
                    string SmtpClient = System.Configuration.ConfigurationManager.AppSettings["SmtpClient"];
                    string WebsiteEmail = System.Configuration.ConfigurationManager.AppSettings["WebsiteEmail"];
                    string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                    string Port = System.Configuration.ConfigurationManager.AppSettings["Port"];
                    string Password = System.Configuration.ConfigurationManager.AppSettings["Password"];

                    MailMessage mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient(SmtpClient);

                    mail.From = new MailAddress(WebsiteEmail);
                    mail.To.Add(AdminEmail);
                    mail.Subject = "New Tour Request";
                    mail.Body = "New Tour Request is created. follow this link to see details http://lankabookin.com/AdminBookingRequest/Details/" + lb_tourrequest.Id;

                    SmtpServer.Port = Convert.ToInt32(Port);
                    SmtpServer.Credentials = new System.Net.NetworkCredential(WebsiteEmail, Password);
                    SmtpServer.EnableSsl = true;

                    SmtpServer.Send(mail);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.TraceError(e.Message);
                }

                return RedirectToAction("Index", "TourRequest", new { display=true });
            }

            Tour tourObject = db.Tour.Find(tourId);
            ViewBag.Tour = tourObject;
            ViewBag.tourId = tourId;
            ViewBag.CityList = new SelectList(db.City, "CityId", "City");
            return View(lb_tourrequest);
        }

        //
        // GET: /TourRequest/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TourRequest lb_tourrequest = db.TourRequest.Find(id);
            if (lb_tourrequest == null)
            {
                return HttpNotFound();
            }
            ViewBag.TourId = new SelectList(db.Tour, "TourId", "Tour", lb_tourrequest.TourId);
            return View(lb_tourrequest);
        }

        //
        // POST: /TourRequest/Edit/5

        [HttpPost]
        public ActionResult Edit(TourRequest lb_tourrequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lb_tourrequest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TourId = new SelectList(db.Tour, "TourId", "Tour", lb_tourrequest.TourId);
            return View(lb_tourrequest);
        }

        //
        // GET: /TourRequest/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TourRequest lb_tourrequest = db.TourRequest.Find(id);
            if (lb_tourrequest == null)
            {
                return HttpNotFound();
            }
            return View(lb_tourrequest);
        }

        //
        // POST: /TourRequest/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TourRequest lb_tourrequest = db.TourRequest.Find(id);
            db.TourRequest.Remove(lb_tourrequest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}