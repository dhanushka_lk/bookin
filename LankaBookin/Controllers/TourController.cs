﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using WebMatrix.WebData;
using System.IO;
using PagedList;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class TourController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /Tour/
        [AllowAnonymous]
        public ActionResult PublicTourList(int categoryId = -1, int? page = 1)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);

            HttpContext.Response.Cookies.Remove("TourRequest");

            if (categoryId != -1)
            {
                var category = db.Category.Find(categoryId);
                if (category != null)
                {
                    Session[Common.SessionVariables.CategoryId.ToString()] = categoryId;
                    return View(category.Tours.Where(t=>t.Deleted!=true).ToPagedList(pageNumber, pageSize));
                }
            }

            return View(db.Tour.Where(t=>t.Deleted!=true).OrderBy(t=>t.TourName).ToPagedList(pageNumber, pageSize));

        }

        //
        // GET: /Tour/
        public ActionResult Index()
        {
            return View(db.Tour.Where(t=>t.Deleted!=true).ToList());
        }


        //
        // GET: /Tour/Details/5

        public ActionResult Details(int id = 0)
        {
            Tour lb_tour = db.Tour.Find(id);
            if (lb_tour == null)
            {
                return HttpNotFound();
            }

            ViewBag.Destinations = (from tour_des in db.TourDestination
                          join tour in db.Tour on tour_des.TourId equals tour.Id
                          join city in db.City on tour_des.CityId equals city.Id
                          where city.Deleted != true && tour.Id == id
                          select city).ToList();

            return View(lb_tour);
        }

        //
        // GET: /Tour/PublicDetails/5
        [AllowAnonymous]
        public ActionResult PublicDetails(int id = 0)
        {
            Tour lb_tour = db.Tour.Find(id);
            if (lb_tour == null)
            {
                return HttpNotFound();
            }

            ViewBag.Destinations = (from tour_des in db.TourDestination
                                    join tour in db.Tour on tour_des.TourId equals tour.Id
                                    join city in db.City on tour_des.CityId equals city.Id
                                    where city.Deleted != true && tour.Id == id
                                    select city).ToList();

            return View(lb_tour);
        }

        //
        // GET: /Tour/Create

        public ActionResult Create(int step=0)
        {
            List<int> steps = new List<int>(new int[] { 1, 2, 3, 4, 5 });
            ViewBag.steps = new SelectList(steps);

            ViewBag.CategoryTypes = db.Category.ToList();
            ViewBag.Cities = new SelectList(db.City, "Id", "CityName");
            return View();
        }

        //
        // POST: /Tour/Create

        [HttpPost]
        public ActionResult Create(Tour lb_tour, HttpPostedFileBase file)
        {
            if (file == null)
            {
                ModelState.AddModelError("uploadMain", "Please upload the image");

            }

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string path = Server.MapPath("~/Images/Tour/");
                    string fullPath = System.IO.Path.Combine(path, pic);

                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    file.SaveAs(fullPath);
                    lb_tour.ImagePath = fullPath;
                }

                int userId = WebSecurity.GetUserId(User.Identity.Name);
                lb_tour.Deleted = false;
                lb_tour.CreatedDate = DateTime.Now;
                lb_tour.CreatedBy = userId;
                db.Tour.Add(lb_tour);
                db.SaveChanges();

                List<Category> categories = db.Category.ToList();
                foreach (Category type in categories)
                {
                    var result = Request[Convert.ToString("Cat" + type.Id)];
                    if (result != null)
                    {
                        if (result.ToString() == "on")
                        {
                            lb_tour.Categories.Add(type);
                            db.SaveChanges();
                        }
                    }

                }

                var districts = Request["Cities"];

                int counter = 1;
                if (districts != null)
                {
                    List<string> cityList = districts.Split(',').ToList();
                    foreach (var d in cityList)
                    {
                        if (!string.IsNullOrEmpty(d))
                        {
                            int districtId = Convert.ToInt32(d);
                            if (districtId == -1)
                                continue;
                            TourDestination tour_des = new TourDestination
                            {
                                TourId = lb_tour.Id,
                                StepId = counter,
                                CityId = districtId,
                                IsDesCity = true,
                                Deleted = false,
                                CreatedBy = userId,
                                CreatedDate = DateTime.Now
                            };
                            db.TourDestination.Add(tour_des);
                            db.SaveChanges();
                            counter++;
                        }
                    }
                }

                return RedirectToAction("Index");
            }

            List<int> steps = new List<int>(new int[] { 1, 2, 3, 4, 5 });
            ViewBag.steps = new SelectList(steps);

            ViewBag.CategoryTypes = db.Category.ToList();
            ViewBag.Cities = new SelectList(db.City, "Id", "CityName");
            return View(lb_tour);
        }

        //
        // GET: /Tour/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Tour lb_tour = db.Tour.Find(id);
            if (lb_tour == null)
            {
                return HttpNotFound();
            }
            return View(lb_tour);
        }

        //
        // POST: /Tour/Edit/5

        [HttpPost]
        public ActionResult Edit(Tour lb_tour)
        {
            if (ModelState.IsValid)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                lb_tour.ModifiedBy = userId;
                lb_tour.ModifiedDate = DateTime.Now;
                db.Entry(lb_tour).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lb_tour);
        }

        //
        // GET: /Tour/Delete/5

        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            Tour lb_tour = db.Tour.Find(id);
            if (lb_tour == null)
            {
                return HttpNotFound();
            }
            return View(lb_tour);
        }

        //
        // POST: /Tour/Delete/5

        [Authorize(Roles = "SuperAdmin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Tour lb_tour = db.Tour.Find(id);
            lb_tour.Deleted = true;
            db.Entry(lb_tour).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}