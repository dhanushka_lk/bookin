﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using WebMatrix.WebData;
using System.Net.Mail;

namespace LankaBookin.Controllers
{
    [Authorize]
    public class BookingRequestController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /BookingRequest/
        [Authorize(Roles = "Public")]
        public ActionResult Index(bool display=false)
        {
            ViewBag.Display = display;
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            var lb_bookingrequest = db.BookingRequest.Where(b => b.UserId == userId && b.complete==true).ToList();
            return View(lb_bookingrequest.ToList());
        }

        //
        // GET: /BookingRequest/Create
        [Authorize(Roles = "Public")]
        public ActionResult Create(int id = 0)
        {
            if (HttpContext.Request.Cookies["TourRequest"] != null)
            {
                int tourId = 0;
                HttpCookie cookie = HttpContext.Request.Cookies.Get("TourRequest");
                var tourid = cookie["stepId"];
                if (!string.IsNullOrEmpty(tourid))
                {
                    tourId = Convert.ToInt32(tourid);
                    var tourDestination = db.TourDestination.Find(tourId);
                    if (tourDestination != null)
                    {
                        ViewBag.CityName = tourDestination.City.CityName;
                        ViewBag.TourName = tourDestination.Tour.TourName;
                        ViewBag.tourId = tourId;
                    }
                }
            }

            int userId = WebSecurity.GetUserId(User.Identity.Name);
            Apartment apartment = db.Apartment.Find(id);

            BookingRequest bookinRequest = new BookingRequest
            {
                OrderDate = DateTime.Now
            };

            var dic = Session["values"];
            if (dic != null)
            {
                Dictionary<string, object> values = (Dictionary<string, object>)dic;

                if (values["checkin"] != null)
                {
                    if (!string.IsNullOrEmpty(values["checkin"].ToString()))
                    {
                        bookinRequest.CheckIn = Convert.ToDateTime(values["checkin"]);
                    }
                    else
                    {
                        bookinRequest.CheckIn = DateTime.Now;
                    }
                }
                else
                {
                    bookinRequest.CheckIn = DateTime.Now;
                }

                if (values["checkout"] != null)
                {
                    if (!string.IsNullOrEmpty(values["checkout"].ToString()))
                    {
                        bookinRequest.CheckOut = Convert.ToDateTime(values["checkout"]);
                    }
                    else
                    {
                        bookinRequest.CheckOut = DateTime.Now;
                    }
                }
                else
                {
                    bookinRequest.CheckOut = DateTime.Now;
                }
            }
            else
            {
                bookinRequest.CheckOut = DateTime.Now;
                bookinRequest.CheckIn = DateTime.Now;
            }

            bookinRequest.ApartmentId = apartment.Id;
            bookinRequest.Apartment = apartment;
            bookinRequest.UserId = userId;

            List<int> rooms = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            ViewBag.RoomsBooked = new SelectList(rooms);

            return View(bookinRequest);
        }

        //
        // POST: /BookingRequest/Create

        [HttpPost]
        [Authorize(Roles = "Public")]
        public ActionResult Create(BookingRequest bookinRequest)
        {
            int tourId = 0;
            if (HttpContext.Request.Cookies["TourRequest"] != null)
            {
                HttpCookie cookie = HttpContext.Request.Cookies.Get("TourRequest");
                var tourid = cookie["stepId"];
                if (!string.IsNullOrEmpty(tourid))
                {
                    tourId = Convert.ToInt32(tourid);
                }
            }

            if (bookinRequest.Price == 0)
            {
                ModelState.AddModelError("Price", "Amount Invalid");
            }

            int userId = WebSecurity.GetUserId(User.Identity.Name);
            bookinRequest.UserId = userId;

            Apartment apartment = db.Apartment.Find(bookinRequest.ApartmentId);
            bookinRequest.Apartment = apartment;

            if (ModelState.IsValid)
            {
                bookinRequest.CreatedBy = userId;
                bookinRequest.CreatedDate = DateTime.Now;
                bookinRequest.Deleted = false;
                if (tourId > 0)
                {
                    bookinRequest.complete = false;
                    db.BookingRequest.Add(bookinRequest);
                    db.SaveChanges();
                    HttpCookie cookie = HttpContext.Request.Cookies.Get("TourRequest");
                    cookie[tourId.ToString()] = bookinRequest.Id.ToString();
                    HttpContext.Response.SetCookie(cookie);
                    return RedirectToAction("Create", "TourRequest", null);
                }
                else
                {
                    bookinRequest.complete = true;
                    db.BookingRequest.Add(bookinRequest);
                    db.SaveChanges();

                    try
                    {
                        string SmtpClient = System.Configuration.ConfigurationManager.AppSettings["SmtpClient"];
                        string WebsiteEmail = System.Configuration.ConfigurationManager.AppSettings["WebsiteEmail"];
                        string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                        string Port = System.Configuration.ConfigurationManager.AppSettings["Port"];
                        string Password = System.Configuration.ConfigurationManager.AppSettings["Password"];

                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient(SmtpClient);

                        mail.From = new MailAddress(WebsiteEmail);
                        mail.To.Add(AdminEmail);
                        mail.Subject = "New Bookin Request";
                        mail.Body = "New Bookin Request is created. follow this link to see details http://lankabookin.com/AdminBookingRequest/Details/" + bookinRequest.Id;

                        SmtpServer.Port = Convert.ToInt32(Port);
                        SmtpServer.Credentials = new System.Net.NetworkCredential(WebsiteEmail, Password);
                        SmtpServer.EnableSsl = true;

                        SmtpServer.Send(mail);
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Trace.TraceError(e.Message); 
                    }

                    return RedirectToAction("Index", "BookingRequest", new { display = true });
                }

            }

            List<int> rooms = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 });
            ViewBag.RoomsBooked = new SelectList(rooms);

            return View(bookinRequest);
        }

        ////
        //// GET: /BookingRequest/Delete/5
        //[Authorize(Roles = "SuperAdmin")]
        //public ActionResult Delete(int id = 0)
        //{
        //    BookingRequest lb_bookingrequest = db.BookingRequest.Find(id);
        //    if (lb_bookingrequest == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(lb_bookingrequest);
        //}

        ////
        //// POST: /BookingRequest/Delete/5

        //[HttpPost, ActionName("Delete")]
        //[Authorize(Roles = "SuperAdmin")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    BookingRequest lb_bookingrequest = db.BookingRequest.Find(id);
        //    db.BookingRequest.Remove(lb_bookingrequest);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult CalculatePrice(int apartmentId, int roomCount)
        {
            Apartment apartment = db.Apartment.Find(apartmentId);
            var price = apartment.PricePerRoom * roomCount;
            return Json(new { price = price }, JsonRequestBehavior.AllowGet);
        }
    }
}