﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using WebMatrix.WebData;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class DistrictController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /District/

        public ActionResult Index()
        {
            return View(db.District.ToList());
        }

        //
        // GET: /District/Details/5

        public ActionResult Details(int id = 0)
        {
            District lb_district = db.District.Find(id);
            if (lb_district == null)
            {
                return HttpNotFound();
            }
            ViewBag.City = db.City.Where(c => c.District.Id == id).ToList();
            return View(lb_district);
        }

        //
        // GET: /District/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /District/Create

        [HttpPost]
        public ActionResult Create(District district)
        {
            if (ModelState.IsValid)
            {
                district.Deleted = false;
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                district.CreatedBy = userId;
                district.CreatedDate = DateTime.Now;
                db.District.Add(district);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(district);
        }

        //
        // GET: /District/Edit/5

        public ActionResult Edit(int id = 0)
        {
            District lb_district = db.District.Find(id);
            if (lb_district == null)
            {
                return HttpNotFound();
            }
            return View(lb_district);
        }

        //
        // POST: /District/Edit/5

        [HttpPost]
        public ActionResult Edit(District district)
        {
            if (ModelState.IsValid)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                district.ModifiedBy = userId;
                district.ModifiedDate = DateTime.Now;
                db.Entry(district).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(district);
        }

        //
        // GET: /District/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            District lb_district = db.District.Find(id);
            if (lb_district == null)
            {
                return HttpNotFound();
            }
            return View(lb_district);
        }

        //
        // POST: /District/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult DeleteConfirmed(int id)
        {
            District lb_district = db.District.Find(id);
            db.District.Remove(lb_district);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}