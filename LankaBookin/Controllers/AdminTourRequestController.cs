﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class AdminTourRequestController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /AdminTourRequest/

        public ActionResult Index()
        {
            var tourrequest = db.TourRequest.Where(r=>r.Deleted!=true);
            return View(tourrequest.ToList());
        }

        //
        // GET: /AdminTourRequest/Details/5

        public ActionResult Details(int id = 0)
        {
            TourRequest tourrequest = db.TourRequest.Find(id);
            if (tourrequest == null)
            {
                return HttpNotFound();
            }
            return View(tourrequest);
        }

        //
        // GET: /AdminTourRequest/Create

        //public ActionResult Create()
        //{
        //    ViewBag.TourId = new SelectList(db.Tour, "Id", "TourName");
        //    ViewBag.UserId = new SelectList(db.User, "Id", "Name");
        //    return View();
        //}

        //
        // GET: /AdminTourRequest/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    TourRequest tourrequest = db.TourRequest.Find(id);
        //    if (tourrequest == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.TourId = new SelectList(db.Tour, "Id", "TourName", tourrequest.TourId);
        //    ViewBag.UserId = new SelectList(db.User, "Id", "Name", tourrequest.UserId);
        //    return View(tourrequest);
        //}

        ////
        //// POST: /AdminTourRequest/Edit/5

        //[HttpPost]
        //public ActionResult Edit(TourRequest tourrequest)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(tourrequest).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.TourId = new SelectList(db.Tour, "Id", "TourName", tourrequest.TourId);
        //    ViewBag.UserId = new SelectList(db.User, "Id", "Name", tourrequest.UserId);
        //    return View(tourrequest);
        //}

        //
        // GET: /AdminTourRequest/Delete/5

        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            TourRequest tourrequest = db.TourRequest.Find(id);
            if (tourrequest == null)
            {
                return HttpNotFound();
            }
            return View(tourrequest);
        }

        //
        // POST: /AdminTourRequest/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult DeleteConfirmed(int id)
        {
            TourRequest tourrequest = db.TourRequest.Find(id);
            tourrequest.Deleted = true;
            db.Entry(tourrequest).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}