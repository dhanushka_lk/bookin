﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using System.IO;
using WebMatrix.WebData;

namespace LankaBookin.Controllers
{
    [Authorize(Roles="Admin,SuperAdmin")]
    public class CityController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /City/

        public ActionResult Index()
        {
            return View(db.City.ToList());
        }

        public ActionResult Details(int id = 0)
        {
            City city = db.City.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }

            return View(city);
        }

        //
        // GET: /City/Create

        public ActionResult Create()
        {
            ViewBag.DistrictId = new SelectList(db.District, "Id", "DistrictName");
            return View();
        }

        //
        // POST: /City/Create

        [HttpPost]
        public ActionResult Create(City city, HttpPostedFileBase file)
        {
            if (city.IsTopDestination != null)
            {
                if ((bool)city.IsTopDestination)
                {
                    if (file == null)
                    {
                        ModelState.AddModelError("Upload", "Image is required for top destinatins");
                    }
                }
            }

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string path = Server.MapPath("~/Images/City/");
                    string fullPath = System.IO.Path.Combine(path, pic);

                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    // file is uploaded
                    file.SaveAs(fullPath);
                    city.ImagePath = fullPath;
                }

                int userId = WebSecurity.GetUserId(User.Identity.Name);
                city.Deleted = false;
                city.CreatedBy = userId;
                city.CreatedDate = DateTime.Now;
                db.City.Add(city);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DistrictId = new SelectList(db.District, "Id", "DistrictName");
            return View(city);
        }

        //
        // GET: /City/Edit/5

        public ActionResult Edit(int id = 0)
        {
            City city = db.City.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            ViewBag.DistrictId = new SelectList(db.District, "Id", "DistrictName", city.DistrictId);
            return View(city);
        }

        //
        // POST: /City/Edit/5

        [HttpPost]
        public ActionResult Edit(City city, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string path = Server.MapPath("~/Images/City/");
                    string fullPath = System.IO.Path.Combine(path, pic);

                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    // file is uploaded
                    file.SaveAs(fullPath);
                    city.ImagePath = fullPath;
                }

                int userId = WebSecurity.GetUserId(User.Identity.Name);
                city.ModifiedBy = userId;
                city.ModifiedDate = DateTime.Now;
                db.Entry(city).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DistrictId = new SelectList(db.District, "Id", "DistrictName", city.DistrictId);
            return View(city);
        }

        //
        // GET: /City/Delete/5
        [Authorize(Roles="SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            City lb_city = db.City.Find(id);
            if (lb_city == null)
            {
                return HttpNotFound();
            }
            return View(lb_city);
        }

        //
        // POST: /City/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult DeleteConfirmed(int id)
        {
            City lb_city = db.City.Find(id);
            db.City.Remove(lb_city);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}