﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class UserRoleController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /UserRole/

        public ActionResult Index()
        {
            return View(db.Roles.ToList());
        }

        //
        // GET: /UserRole/Details/5

        public ActionResult Details(int id = 0)
        {
            Role webpages_roles = db.Roles.Find(id);
            if (webpages_roles == null)
            {
                return HttpNotFound();
            }

            ViewBag.Users = (from ur in db.UsersInRoles
                             join role in db.Roles on ur.RoleId equals role.RoleId
                             join user in db.User on ur.UserId equals user.Id
                             where role.RoleId == id
                             select user).ToList();

            return View(webpages_roles);
        }

        //
        // GET: /UserRole/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /UserRole/Create

        [HttpPost]
        public ActionResult Create(Role webpages_roles)
        {
            if (string.IsNullOrEmpty(webpages_roles.RoleName))
            {
                ModelState.AddModelError("", "Please enter role name");
            }

            if (ModelState.IsValid)
            {
                db.Roles.Add(webpages_roles);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(webpages_roles);
        }

        //
        // GET: /UserRole/Edit/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Edit(int id = 0)
        {
            Role webpages_roles = db.Roles.Find(id);
            if (webpages_roles == null)
            {
                return HttpNotFound();
            }
            return View(webpages_roles);
        }

        //
        // POST: /UserRole/Edit/5

        [HttpPost]
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Edit(Role webpages_roles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(webpages_roles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(webpages_roles);
        }

        //
        // GET: /UserRole/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            Role webpages_roles = db.Roles.Find(id);
            if (webpages_roles == null)
            {
                return HttpNotFound();
            }
            return View(webpages_roles);
        }

        //
        // POST: /UserRole/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Role webpages_roles = db.Roles.Find(id);
            db.Roles.Remove(webpages_roles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}