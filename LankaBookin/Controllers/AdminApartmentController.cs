﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using LankaBookin.Common;
using WebMatrix.WebData;
using System.IO;

namespace LankaBookin.Controllers
{
    [Authorize(Roles = "Admin,SuperAdmin")]
    public class AdminApartmentController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /AdminApartment/

        public ActionResult Index()
        {
            var apartment = db.Apartment.Where(a=>a.Deleted!=true);
            return View(apartment.ToList());
        }

        //
        // GET: /AdminApartment/Details/5

        public ActionResult Details(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }
            return View(apartment);
        }

        //
        // GET: /AdminApartment/Create

        public ActionResult Create()
        {
            string newRef = string.Empty;
            var lastRefA = db.Apartment.OrderByDescending(a => a.RefNo).FirstOrDefault();
            if (lastRefA != null)
            {
                string lastRef = lastRefA.RefNo;
                int value = Convert.ToInt32(lastRef.Substring(6));
                newRef = string.Format("{0}{1:00000}", "bookin", value + 1);
            }
            else
            {
                int value = 0;
                newRef = string.Format("{0}{1:00000}", "bookin", value + 1);
            }

            ViewBag.RefNo = newRef;

            int internalUserType = (int)UserRoles.Internal;

            var internalUsers = (from ur in db.UsersInRoles
                                 join role in db.Roles on ur.RoleId equals role.RoleId
                                 join user in db.User on ur.UserId equals user.Id
                                 where role.RoleId == internalUserType
                                 select user).ToList();

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
            ViewBag.DistrictId = new SelectList(db.District, "Id", "DistrictName");

            ViewBag.UserId = new SelectList(internalUsers, "Id", "Name");
            List<AccomadationType> accomadationTypes = db.AccomadationType.ToList();
            ViewBag.AccomadationTypes = accomadationTypes;
            List<Facility> facilities = db.Facility.OrderBy(f=>f.FacilityCategoryId).ToList();
            ViewBag.Facilities = facilities;

            List<int> headCount = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
            ViewBag.MaxHeadCount = new SelectList(headCount);

            List<int> bedRoom = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            ViewBag.NoOfBedrooms = new SelectList(bedRoom);

            List<int> bathRooms = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            ViewBag.NoOfBathrooms = new SelectList(bathRooms);

            return View();
        }

        //
        // POST: /AdminApartment/Create

        [HttpPost]
        public ActionResult Create(Apartment apartment)
        {
            apartment.Name = apartment.Name.Trim();

            if (apartment.Name.IndexOfAny(System.IO.Path.GetInvalidPathChars()) >= 0)
            {
                ModelState.AddModelError("", "Apartment name has invalid Characters");
            }

            apartment.Name = apartment.Name.Replace("/", " ");

            if (ModelState.IsValid)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);

                if (apartment.UserId == 0)
                {
                    apartment.UserId = userId;
                }

                var refNo = Request["RefNo"];

                apartment.RefNo = refNo;
                apartment.Deleted = false;
                apartment.CreatedDate = DateTime.Now;
                apartment.CreatedBy = userId;
                apartment.ModifiedDate = DateTime.Now;
                apartment.ModifiedBy = userId;
                db.Apartment.Add(apartment);
                db.SaveChanges();

                List<AccomadationType> accomadationTypes = db.AccomadationType.ToList();
                foreach (AccomadationType type in accomadationTypes)
                {
                    var result = Request[Convert.ToString("Acc" + type.Id)];
                    if (result != null)
                    {
                        if (result.ToString() == "on")
                        {
                            apartment.Accomadations.Add(type);
                            db.SaveChanges();
                        }
                    }

                }

                List<Facility> facilities = db.Facility.ToList();
                foreach (Facility type in facilities)
                {
                    var result = Request[Convert.ToString("Fac" + type.Id)];
                    if (result != null)
                    {
                        if (result.ToString() == "on")
                        {
                            apartment.Facilities.Add(type);
                            db.SaveChanges();
                        }
                    }

                }

                return RedirectToAction("UploadImages", new { id = apartment.Id });
            }

            string newRef = string.Empty;
            var lastRefA = db.Apartment.OrderByDescending(a => a.RefNo).FirstOrDefault();
            if (lastRefA != null)
            {
                string lastRef = lastRefA.RefNo;
                int value = Convert.ToInt32(lastRef.Substring(6));
                newRef = string.Format("{0}{1:00000}", "bookin", value + 1);
            }
            else
            {
                int value = 0;
                newRef = string.Format("{0}{1:00000}", "bookin", value + 1);
            }

            ViewBag.RefNo = newRef;

            int internalUserType = (int)UserRoles.Internal;

            var internalUsers = (from ur in db.UsersInRoles
                                 join role in db.Roles on ur.RoleId equals role.RoleId
                                 join user in db.User on ur.UserId equals user.Id
                                 where role.RoleId == internalUserType
                                 select user).ToList();

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
            ViewBag.DistrictId = new SelectList(db.District, "Id", "DistrictName");

            ViewBag.UserId = new SelectList(internalUsers, "Id", "Name");
            List<AccomadationType> accomadationType = db.AccomadationType.ToList();
            ViewBag.AccomadationTypes = accomadationType;
            List<Facility> facility = db.Facility.OrderBy(f => f.FacilityCategoryId).ToList();
            ViewBag.Facilities = facility;

            List<int> headCount = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
            ViewBag.MaxHeadCount = new SelectList(headCount);

            List<int> bedRoom = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            ViewBag.NoOfBedrooms = new SelectList(bedRoom);

            List<int> bathRooms = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            ViewBag.NoOfBathrooms = new SelectList(bathRooms);

            return View(apartment);
        }

        //
        // GET: /AdminApartment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }

            int internalUserType = (int)UserRoles.Internal;

            var internalUsers = (from ur in db.UsersInRoles
                                 join role in db.Roles on ur.RoleId equals role.RoleId
                                 join user in db.User on ur.UserId equals user.Id
                                 where role.RoleId == internalUserType
                                 select user).ToList();

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);

            int districtId=1;
            var district=db.District.Find(apartment.City.DistrictId);
            if(district!=null)
            {
                districtId=district.Id;
            }

            ViewBag.DistrictId = districtId;
            ViewBag.Districts = db.District;

            List<AccomadationType> accomadationType = db.AccomadationType.ToList();
            ViewBag.AccomadationTypes = accomadationType;

            List<Facility> facility = db.Facility.OrderBy(f => f.FacilityCategoryId).ToList();
            ViewBag.Facilities = facility;

            ViewBag.UserId = new SelectList(internalUsers, "Id", "Name", apartment.UserId);

            return View(apartment);
        }

        //
        // POST: /AdminApartment/Edit/5

        [HttpPost]
        public ActionResult Edit(Apartment apartment, string[] selectedAccomadationTypes, string[] selectedFacilities, HttpPostedFileBase file)
        {
            apartment.Name = apartment.Name.Trim();

            if (apartment.Name.IndexOfAny(System.IO.Path.GetInvalidPathChars()) >= 0)
            {
                ModelState.AddModelError("", "Apartment name has invalid Characters");
            }

            if (ModelState.IsValid)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                apartment.ModifiedDate = DateTime.Now;
                apartment.ModifiedBy = userId;
                db.Entry(apartment).State = EntityState.Modified;
                db.SaveChanges();

                ICollection<AccomadationType> accomadationTypes = new HashSet<AccomadationType>();
                if (selectedAccomadationTypes != null)
                {
                    foreach (var accomadationType in db.AccomadationType.ToList())
                    {
                        if (selectedAccomadationTypes.ToList().Contains(accomadationType.Id.ToString()))
                        {
                            accomadationType.Apartments.Add(apartment);
                            accomadationTypes.Add(accomadationType);
                        }
                        else
                        {
                            accomadationType.Apartments.Remove(apartment);
                        }
                    }
                }
                else
                {
                    foreach (var accomadationType in db.AccomadationType.ToList())
                    {
                        accomadationType.Apartments.Remove(apartment);
                    }
                }

                apartment.Accomadations = accomadationTypes;
                db.SaveChanges();

                ICollection<Facility> facilities = new HashSet<Facility>();

                if (selectedFacilities != null)
                {
                    foreach (var facility in db.Facility.ToList())
                    {
                        if (selectedFacilities.ToList().Contains(facility.Id.ToString()))
                        {
                            facility.Apartments.Add(apartment);
                            facilities.Add(facility);
                        }
                        else
                        {
                            facility.Apartments.Remove(apartment);
                        }
                    }

                }
                else
                {
                    foreach (var facility in db.Facility.ToList())
                    {
                        facility.Apartments.Remove(apartment);
                    }
                }

                apartment.Facilities = facilities;
                db.SaveChanges();


                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string path = Server.MapPath("~/Images/Apartment/" + apartment.Name);
                    string fullPath = System.IO.Path.Combine(path, pic);

                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    // file is uploaded
                    file.SaveAs(fullPath);
                    apartment.MainImagePath = fullPath;
                    db.SaveChanges();

                    ImageCrop.CropAndOverwrite(fullPath);

                }

                return RedirectToAction("Index");
            }

            int internalUserType = (int)UserRoles.Internal;

            var internalUsers = (from ur in db.UsersInRoles
                                 join role in db.Roles on ur.RoleId equals role.RoleId
                                 join user in db.User on ur.UserId equals user.Id
                                 where role.RoleId == internalUserType
                                 select user).ToList();

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);

            List<AccomadationType> accomadationTypess = db.AccomadationType.ToList();
            ViewBag.AccomadationTypes = accomadationTypess;

            List<Facility> facilitys = db.Facility.ToList();
            ViewBag.Facilities = facilitys;

            ViewBag.UserId = new SelectList(internalUsers, "Id", "Name", apartment.UserId);
            return View(apartment);
        }

        //
        // GET: /AdminApartment/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        public ActionResult Delete(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }
            return View(apartment);
        }

        //
        // POST: /AdminApartment/Delete/5
        [Authorize(Roles = "SuperAdmin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Apartment apartment = db.Apartment.Find(id);
            apartment.Deleted = true;
            db.Entry(apartment).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult UploadImages(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = id;
            ViewBag.Name = apartment.Name;

            return View();
        }


    }
}