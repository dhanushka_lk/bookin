﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LankaBookin.Models;
using LankaBookin.DAL;
using WebMatrix.WebData;
using System.IO;
using PagedList;
using LankaBookin.Common;
using Newtonsoft.Json;
using GoogleMaps.LocationServices;
using System.Net.Http;
using LankaBookin.CurrencyConverter;
using ImageResizer;

namespace LankaBookin.Controllers
{
    public class ApartmentController : Controller
    {
        private LankaBookinEntities db = new LankaBookinEntities();

        //
        // GET: /Apartment/
        [Authorize]
        [Authorize(Roles = "Internal")]
        public ActionResult Index()
        {
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            return View(db.Apartment.Where(a => a.UserId == userId && a.Deleted != true).ToList());
        }

        [Authorize]
        public ActionResult ApartmentList(DateTime? checkin = null, DateTime? checkout = null,int desId=0)
        {
            ViewBag.DesId = desId;
            return PartialView(db.Apartment.Where(a => a.Deleted != true).ToList());
        }

        public ActionResult CancelTour()
        {
            if (HttpContext.Request.Cookies["TourRequest"] != null)
            {
                HttpCookie cookie = HttpContext.Request.Cookies.Get("TourRequest");
                cookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Response.SetCookie(cookie);
            }

            return RedirectToAction("Index","Home");
        }

        //
        // GET: /Apartment/
        [AllowAnonymous]
        [OutputCache(Duration = int.MaxValue, VaryByParam = "search;checkin;checkout;no_rooms;search_prop_type;page;cityId;districtId;minPrice;maxPrice;tourId")]
        public ActionResult Search(string search = "", DateTime? checkin = null, DateTime? checkout = null, int no_rooms = 0, int search_prop_type = 0, int? page = 0, int cityId = 0, int districtId = 0, int minPrice = 0, int maxPrice = 0,int tourId=0)
        {
            if (tourId != 0 && HttpContext.Request.Cookies["TourRequest"] != null)
            {
                var tourDestination = db.TourDestination.Find(tourId);
                if (tourDestination != null)
                {
                    ViewBag.CityName = tourDestination.City.CityName;
                    ViewBag.TourName = tourDestination.Tour.TourName;
                    ViewBag.tourId = tourId;

                    HttpCookie cookie = HttpContext.Request.Cookies.Get("TourRequest");
                    cookie["stepId"] = tourId.ToString();
                    HttpContext.Response.Cookies.Remove("TourRequest");
                    HttpContext.Response.SetCookie(cookie);
                }
            }
            else if (HttpContext.Request.Cookies["TourRequest"] != null)
            {
                HttpCookie cookie = HttpContext.Request.Cookies.Get("TourRequest");
                var tourid = cookie["stepId"];
                if (!string.IsNullOrEmpty(tourid))
                {
                    tourId = Convert.ToInt32(tourid);
                    var tourDestination = db.TourDestination.Find(tourId);
                    if (tourDestination != null)
                    {
                        ViewBag.CityName = tourDestination.City.CityName;
                        ViewBag.TourName = tourDestination.Tour.TourName;
                        ViewBag.tourId = tourId;
                    }
                }
            }

            List<Facility> facilityList = new List<Facility>();

            List<Facility> facilityFullList = db.Facility.Where(f=>f.Priority!=3).ToList();
            foreach (Facility type in facilityFullList)
            {
                var result = Request[Convert.ToString("Fac" + type.Id)];
                if (result != null)
                {
                    if (result.ToString() == "on")
                    {
                        facilityList.Add(type);
                    }
                }

            }

            if (page == 0)
            {
                Session.Remove("values");

                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic.Add("min", minPrice);
                dic.Add("max", maxPrice);
                dic.Add("facilityList", facilityList);
                dic.Add("search", search);
                dic.Add("checkin", checkin);
                dic.Add("checkout", checkout);
                dic.Add("search_prop_type", search_prop_type);
                dic.Add("no_rooms", no_rooms);
                dic.Add("districtId", districtId);
                dic.Add("cityId", cityId);
                Session.Add("values", dic);
            }
            else
            {
                var dic = Session["values"];
                if (dic != null)
                {
                    Dictionary<string, object> values = (Dictionary<string, object>)dic;

                    if (values["min"]!=null)
                    {
                        if (!string.IsNullOrEmpty(values["min"].ToString()))
                        {
                            minPrice = Convert.ToInt32(values["min"]);
                        }
                    }

                    if (values["max"] != null)
                    {
                        if (!string.IsNullOrEmpty(values["max"].ToString()))
                        {
                            maxPrice = Convert.ToInt32(values["max"]);
                        }
                    }

                    if (values["facilityList"]!=null)
                    {
                        facilityList = (List<Facility>)values["facilityList"];
                    }

                    if (values["search"] != null)
                    {
                        if (!string.IsNullOrEmpty(values["search"].ToString()))
                        {
                            search = Convert.ToString(values["search"]);
                        }
                    }

                    if (values["checkin"] != null)
                    {
                        if (!string.IsNullOrEmpty(values["checkin"].ToString()))
                        {
                            checkin = Convert.ToDateTime(values["checkin"]);
                        }
                    }

                    if (values["checkout"] != null)
                    {
                        if (!string.IsNullOrEmpty(values["checkout"].ToString()))
                        {
                            checkout = Convert.ToDateTime(values["checkout"]);
                        }
                    }

                    if (values["search_prop_type"] != null)
                    {
                        if (!string.IsNullOrEmpty(values["search_prop_type"].ToString()))
                        {
                            search_prop_type = Convert.ToInt32(values["search_prop_type"]);
                        }
                    }

                    if (values["no_rooms"] != null)
                    {
                        if (!string.IsNullOrEmpty(values["no_rooms"].ToString()))
                        {
                            no_rooms = Convert.ToInt32(values["no_rooms"]);
                        }
                    }

                    if (values["districtId"] != null)
                    {
                        if (!string.IsNullOrEmpty(values["districtId"].ToString()))
                        {
                            districtId = Convert.ToInt32(values["districtId"]);
                        }
                    }

                    if (values["cityId"] != null)
                    {
                        if (!string.IsNullOrEmpty(values["cityId"].ToString()))
                        {
                            cityId = Convert.ToInt32(values["cityId"]);
                        }
                    }
                }
            }

            List<Apartment> apartments;
            List<Apartment> apartmentList;

            if (!string.IsNullOrEmpty(search) && !search.Contains("Search By Name"))
            {
                apartments = db.Apartment.Where(a => a.Deleted != true && a.Name.Contains(search)).OrderByDescending(a=>a.Rate).ToList();
                apartmentList = db.Apartment.Where(a => a.Deleted != true && a.Name.Contains(search)).OrderByDescending(a => a.Rate).ToList();
            }
            else
            {
                apartments = db.Apartment.Where(a => a.Deleted != true).OrderByDescending(a => a.Rate).ToList();
                apartmentList = db.Apartment.Where(a => a.Deleted != true).OrderByDescending(a => a.Rate).ToList();
            }

            var cities = db.District.Where(d => d.Id == districtId).Select(d => d.City.Select(c=>c.Id)).FirstOrDefault();

            foreach (var apartment in apartments)
            {
                if (checkin != null && checkout != null)
                {
                    DateTime date=(DateTime)checkin;
                    while(date<=checkout)
                    {
                        var result = db.RoomsBook.Where(r => r.ApartmentId == apartment.Id && r.Date == date).Select(r=>r.booked);
                        if (result != null)
                        {
                            apartmentList.Remove(apartment);
                        }

                        date = date.AddDays(1);
                    }

                }

                if (maxPrice > minPrice)
                {
                    if (apartment.IsPerRoom)
                    {
                        if (apartment.PricePerRoom < minPrice || apartment.PricePerRoom > maxPrice)
                        {
                            apartmentList.Remove(apartment);
                        }
                    }
                    else
                    {
                        if (apartment.WholePricePerDay < minPrice || apartment.WholePricePerDay > maxPrice)
                        {
                            apartmentList.Remove(apartment);
                        }
                    }
                }

                var accType = db.AccomadationType.Find(search_prop_type);
                if (accType != null)
                {
                    if (!apartment.Accomadations.Contains(accType))
                    {
                        apartmentList.Remove(apartment);
                    }
                }

                if (facilityList.Count > 0)
                {
                    foreach (var index in facilityList)
                    {
                        if (!apartment.Facilities.Contains(index))
                        {
                            apartmentList.Remove(apartment);
                        }

                    }
                }

                if (no_rooms > 0)
                {
                    if (apartment.NoOfBedrooms != no_rooms)
                    {
                        apartmentList.Remove(apartment);
                    }
                }

                if (cityId > 0)
                {
                    if (apartment.CityId != cityId)
                    {
                        apartmentList.Remove(apartment);
                    }
                }

                if (cityId == 0 && districtId > 0)
                {
                    if (cities != null && cities.Count()>0)
                    {
                        if (!cities.Contains(apartment.CityId))
                        {
                            apartmentList.Remove(apartment);
                        }
                    }
                }

            }

            ViewBag.AccomadationTypes = new SelectList(db.AccomadationType, "Id", "AccomadationName");
            ViewBag.Districts = new SelectList(db.District.OrderBy(d=>d.DistrictName), "Id", "DistrictName");
            ViewBag.CityId = new SelectList(db.City.OrderBy(c=>c.CityName), "Id", "CityName");

            List<Facility> facilities = db.Facility.OrderBy(f=>f.FacilityCategoryId).Where(f=>f.Priority!=3).ToList();
            ViewBag.Facilities = facilities;
            ViewBag.PreFacilities = facilityList;

            List<int> rooms = new List<int>(new int[] { 1, 2, 3,4,5,6,7,8,9,10,11,12,13,14,15 });
            ViewBag.no_rooms = new SelectList(rooms);

            ViewBag.minPrice = minPrice;
            ViewBag.maxPrice = maxPrice;
            ViewBag.lkrValue = getConversionRate("USD", "LKR");

            int pageSize = 5;
            if (page == 0)
            {
                page = 1;
            }
            int pageNumber = (page ?? 1);

            string titleName = "hotels-resorts-villas-sri-lanka | Lankabookin";

            if (cityId > 0)
            {
                var cityResult = db.City.Find(cityId);
                if (cityResult != null)
                {
                    var cityName = cityResult.CityName;
                    titleName = "hotels-resorts-villas-" + cityName + "-sri-lanka| Lankabookin";
                }
            }
            else if (districtId > 0)
            {
                var districtResult = db.District.Find(districtId);
                if (districtResult != null)
                {
                    var districtName = districtResult.DistrictName;
                    titleName = "hotels-resorts-villas-" + districtName + "-sri-lanka| Lankabookin";

                }
            }

            ViewBag.Title = titleName;
            return View(apartmentList.ToPagedList(pageNumber, pageSize));
        }

        [AllowAnonymous]
        public ActionResult DetailsPartial(int id = 0)
        {            
            Apartment lb_apartment = db.Apartment.Find(id);
            if (lb_apartment == null)
            {
                return HttpNotFound();
            }

            return PartialView(lb_apartment); ;
        }

        //
        // GET: /Apartment/Details/5
        [OutputCache(Duration = int.MaxValue, VaryByParam = "id")]
        public ActionResult Details(int id = 0)
        {
            if (HttpContext.Request.Cookies["TourRequest"] != null)
            {
                int tourId = 0;
                HttpCookie cookie = HttpContext.Request.Cookies.Get("TourRequest");
                var tourid = cookie["stepId"];
                if (!string.IsNullOrEmpty(tourid))
                {
                    tourId = Convert.ToInt32(tourid);
                    var tourDestination = db.TourDestination.Find(tourId);
                    if (tourDestination != null)
                    {
                        ViewBag.CityName = tourDestination.City.CityName;
                        ViewBag.TourName = tourDestination.Tour.TourName;
                        ViewBag.tourId = tourId;
                    }
                }
            }

            int userId = WebSecurity.GetUserId(User.Identity.Name);
            Apartment lb_apartment = db.Apartment.Find(id);
            if (lb_apartment == null)
            {
                return HttpNotFound();
            }

            var rate = lb_apartment.Rating.Where(r => r.UserId == userId).FirstOrDefault();

            if (rate != null)
            {
                ViewBag.UserRate = rate.Rate;
            }

            if (string.IsNullOrEmpty(lb_apartment.Title))
            {
                ViewBag.Title = lb_apartment.Name;
            }
            else
            {
                ViewBag.Title = lb_apartment.Title;
            }

            ViewBag.Description = lb_apartment.MetaDescription;
            ViewBag.lkrValue = getConversionRate("USD", "LKR");
            return View(lb_apartment);
        }

        //
        // GET: /Apartment/Details/5
        [Authorize(Roles = "Internal")]
        public ActionResult InternalDetails(int id = 0)
        {
            Apartment lb_apartment = db.Apartment.Find(id);
            if (lb_apartment == null)
            {
                return HttpNotFound();
            }
            return View(lb_apartment);
        }

        //
        // GET: /Apartment/Create
        [Authorize(Roles = "Internal")]
        public ActionResult Create()
        {
            string newRef = string.Empty;
            var lastRefA = db.Apartment.OrderByDescending(a => a.RefNo).FirstOrDefault();
            if (lastRefA != null)
            {
                string lastRef = lastRefA.RefNo;
                int value = Convert.ToInt32(lastRef.Substring(6));
                newRef = string.Format("{0}{1:00000}", "bookin", value + 1);
            }
            else
            {
                int value = 0;
                newRef = string.Format("{0}{1:00000}", "bookin", value + 1);
            }

            ViewBag.RefNo = newRef;

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName");
            ViewBag.DistrictId = new SelectList(db.District, "Id", "DistrictName");

            List<AccomadationType> accomadationTypes = db.AccomadationType.ToList();
            ViewBag.AccomadationTypes = accomadationTypes;
            List<Facility> facilities = db.Facility.ToList();
            ViewBag.Facilities = facilities;

            List<int> headCount = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30 });
            ViewBag.MaxHeadCount = new SelectList(headCount);

            List<int> bedRoom = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
            ViewBag.NoOfBedrooms = new SelectList(bedRoom);

            List<int> bathRooms = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
            ViewBag.NoOfBathrooms = new SelectList(bathRooms);

            return View();
        }

        //
        // POST: /Apartment/Create

        [HttpPost]
        [Authorize(Roles = "Internal")]
        public ActionResult Create(Apartment apartment)
        {
            apartment.Name = apartment.Name.Trim();

            if (apartment.Name.IndexOfAny(System.IO.Path.GetInvalidPathChars()) >= 0)
            {
                ModelState.AddModelError("", "Apartment name has invalid Characters");
            }

            apartment.Name = apartment.Name.Replace("/", " ");

            if (ModelState.IsValid)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);

                if (apartment.UserId == 0)
                {
                    apartment.UserId = userId;
                }

                var refNo = Request["RefNo"];

                apartment.RefNo = refNo;
                apartment.Deleted = false;
                apartment.CreatedDate = DateTime.Now;
                apartment.CreatedBy = userId;
                apartment.ModifiedDate = DateTime.Now;
                apartment.ModifiedBy = userId;
                db.Apartment.Add(apartment);
                db.SaveChanges();

                List<AccomadationType> accomadationTypes = db.AccomadationType.ToList();
                foreach (AccomadationType type in accomadationTypes)
                {
                    var result = Request[Convert.ToString("Acc"+type.Id)];
                    if (result != null)
                    {
                        if (result.ToString() == "on")
                        {
                            apartment.Accomadations.Add(type);
                            db.SaveChanges();
                        }
                    }

                }

                List<Facility> facilities = db.Facility.ToList();
                foreach (Facility type in facilities)
                {
                    var result = Request[Convert.ToString("Fac" + type.Id)];
                    if (result != null)
                    {
                        if (result.ToString() == "on")
                        {
                            apartment.Facilities.Add(type);
                            db.SaveChanges();
                        }
                    }

                }

                return RedirectToAction("UploadImages", new {id=apartment.Id });
            }

            string newRef = string.Empty;
            var lastRefA = db.Apartment.OrderByDescending(a => a.RefNo).FirstOrDefault();
            if (lastRefA != null)
            {
                string lastRef = lastRefA.RefNo;
                int value = Convert.ToInt32(lastRef.Substring(6));
                newRef = string.Format("{0}{1:00000}", "bookin", value + 1);
            }
            else
            {
                int value = 0;
                newRef = string.Format("{0}{1:00000}", "bookin", value + 1);
            }

            ViewBag.RefNo = newRef;

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);
            ViewBag.DistrictId = new SelectList(db.District, "Id", "DistrictName");

            List<AccomadationType> accomadationType = db.AccomadationType.ToList();
            ViewBag.AccomadationTypes = accomadationType; 
            List<Facility> facility = db.Facility.OrderBy(f=>f.FacilityCategoryId).ToList();
            ViewBag.Facilities = facility;

            List<int> headCount = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 });
            ViewBag.MaxHeadCount = new SelectList(headCount);

            List<int> bedRoom = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            ViewBag.NoOfBedrooms = new SelectList(bedRoom);

            List<int> bathRooms = new List<int>(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
            ViewBag.NoOfBathrooms = new SelectList(bathRooms);

            return View(apartment);
        }

        //
        // GET: /Apartment/Edit/5
        [Authorize(Roles = "Internal")]
        public ActionResult Edit(int id = 0)
        {
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            Apartment apartment = db.Apartment.Where(a => a.Id == id && a.UserId == userId).SingleOrDefault();
            
            if (apartment == null)
            {
                return HttpNotFound();
            }

            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);

            List<AccomadationType> accomadationType = db.AccomadationType.ToList();
            ViewBag.AccomadationTypes = accomadationType;

            List<Facility> facility = db.Facility.ToList();
            ViewBag.Facilities = facility;
            return View(apartment);
        }

        //
        // POST: /Apartment/Edit/5

        [HttpPost]
        [Authorize(Roles = "Internal")]
        public ActionResult Edit(Apartment apartment, string[] selectedAccomadationTypes, string[] selectedFacilities, HttpPostedFileBase file)
        {
            apartment.Name = apartment.Name.Trim();

            if (apartment.Name.IndexOfAny(System.IO.Path.GetInvalidPathChars()) >= 0)
            {
                ModelState.AddModelError("", "Apartment name has invalid Characters");
            }

            if (ModelState.IsValid)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                apartment.ModifiedDate = DateTime.Now;
                apartment.ModifiedBy = userId;
                db.Entry(apartment).State = EntityState.Modified;
                db.SaveChanges();

                ICollection<AccomadationType> accomadationTypes = new HashSet<AccomadationType>();
                if (selectedAccomadationTypes != null)
                {
                    foreach (var accomadationType in db.AccomadationType.ToList())
                    {
                        if (selectedAccomadationTypes.ToList().Contains(accomadationType.Id.ToString()))
                        {
                            accomadationType.Apartments.Add(apartment);
                            accomadationTypes.Add(accomadationType);
                        }
                        else
                        {
                            accomadationType.Apartments.Remove(apartment);
                        }
                    }
                }
                else
                {
                    foreach (var accomadationType in db.AccomadationType.ToList())
                    {
                        accomadationType.Apartments.Remove(apartment);
                    }
                }

                apartment.Accomadations = accomadationTypes;
                db.SaveChanges();

                ICollection<Facility> facilities = new HashSet<Facility>();

                if (selectedFacilities != null)
                {
                    foreach (var facility in db.Facility.ToList())
                    {
                        if (selectedFacilities.ToList().Contains(facility.Id.ToString()))
                        {
                            facility.Apartments.Add(apartment);
                            facilities.Add(facility);
                        }
                        else
                        {
                            facility.Apartments.Remove(apartment);
                        }
                    }

                }
                else
                {
                    foreach (var facility in db.Facility.ToList())
                    {
                        facility.Apartments.Remove(apartment);
                    }
                }

                apartment.Facilities = facilities;
                db.SaveChanges();

                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);
                    string path = Server.MapPath("~/Images/Apartment/" + apartment.Name);
                    string fullPath = System.IO.Path.Combine(path, pic);

                    if (!Directory.Exists(path))
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path);
                    }

                    // file is uploaded
                    file.SaveAs(fullPath);
                    apartment.MainImagePath = fullPath;
                    db.SaveChanges();

                    ImageCrop.CropAndOverwrite(fullPath);

                }

                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(db.City, "Id", "CityName", apartment.CityId);
            return View(apartment);
        }

        //
        // GET: /Apartment/Delete/5
        [Authorize(Roles = "Internal")]
        public ActionResult Delete(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }
            return View(apartment);
        }

        //
        // GET: /Apartment/WishList/5
        [OutputCache(Duration = int.MaxValue, VaryByParam = "id")]
        public ActionResult ViewMap(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = id;
            ViewBag.Name = apartment.Name;

            ViewBag.Lat = apartment.Lat;
            ViewBag.Lang = apartment.Lan;
            return View();
        }

        [Authorize(Roles = "Public")]
        public ActionResult WishList(int id = 0)
        {
            if (id != 0)
            {
                if (User.IsInRole(Common.UserRoles.Public.ToString()))
                {
                    int userId = WebSecurity.GetUserId(User.Identity.Name);
                    if (userId != -1)
                    {
                        UserWishList wishList = new UserWishList
                        {
                            ApartmentId = id,
                            UserId = userId
                        };
                        db.UserWishList.Add(wishList);
                        db.SaveChanges();
                    }
                }
            }

            return RedirectToAction("Search");
        }

        [Authorize(Roles = "Public")]
        public ActionResult AddReview(string review, int id = 0)
        {
            if (id != 0)
            {
                if (!string.IsNullOrEmpty(review))
                {
                    int userId = WebSecurity.GetUserId(User.Identity.Name);
                    if (userId != -1)
                    {
                        Review reviewObject = new Review
                        {
                            ApartmentId = id,
                            UserId = userId,
                            ReviewNote=review,
                            CreatedDate=DateTime.Now
                        };

                        var apartment = db.Apartment.Find(id);
                        apartment.Reviews.Add(reviewObject);
                        db.SaveChanges();
                    }
                }
            }

            return RedirectToAction("Details", new {id=id });
        }

        [Authorize(Roles = "Public")]
        public ActionResult AddRating(int rate,int apartment)
        {
            if (rate != 0 && apartment != 0)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                var preRate = db.Rating.Where(r => r.UserId == userId && r.ApartmentId == apartment).FirstOrDefault();
                if (preRate != null)
                {
                    preRate.Rate = rate;
                    preRate.ModifiedBy = userId;
                    preRate.ModifiedDate = DateTime.Now;
                    db.SaveChanges();
                    CalculateRating(apartment);
                }
                else
                {
                    var date = DateTime.Now;
                    Rating newRate = new Rating
                    {
                        UserId = userId,
                        ApartmentId = apartment,
                        Rate = rate,
                        CreatedBy = userId,
                        CreatedDate = date,
                        Deleted = false
                    };

                    db.Rating.Add(newRate);
                    db.SaveChanges();
                    CalculateRating(apartment);
                }
            }
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Public")]
        public ActionResult CalculateRating(int apartment)
        {
            Apartment lb_apartment = db.Apartment.Find(apartment);
            if (lb_apartment != null)
            {
                int rate = 0;
                var rating = lb_apartment.Rating.Select(r => r.Rate).ToList();
                if (rating.Count > 0)
                {
                    rate = rating.Sum();
                    var avarage = rate * 1.0 / rating.Count;
                    lb_apartment.Rate = Convert.ToDecimal(avarage);
                    db.SaveChanges();
                }
            }

            return RedirectToAction("Details", new { id = apartment });
        }

        //
        // POST: /Apartment/Delete/5

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Internal")]
        public ActionResult DeleteConfirmed(int id)
        {
            Apartment apartment = db.Apartment.Find(id);
            apartment.Deleted = true;
            db.Entry(apartment).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult GetApartmentName(int apartmentId)
        {
            Apartment apartment = db.Apartment.Find(apartmentId);
            return Json(new { name = apartment.Name, price = apartment.PricePerRoom, isPerRoom = apartment.IsPerRoom }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCityByDisId(int districtid)
        {
            List<City> objcity = db.City.Where(c => c.DistrictId == districtid).ToList();
            SelectList obgcity = new SelectList(objcity, "Id", "CityName", 0);
            return Json(obgcity, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AutoCompleteCity(string term)
        {
            var result = (from r in db.City
                          where r.CityName.ToLower().Contains(term.ToLower())
                          select new { r.CityName }).Distinct();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult CheckValidApartmentName(string name)
        {
            name = name.Trim();
            var result = db.Apartment.Where(u => u.Name == name).FirstOrDefault();
            if (result != null)
            {
                return Json(new { valid = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { valid = true }, JsonRequestBehavior.AllowGet);

            }
        }

        public ActionResult UploadImages(int id = 0)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = id;
            ViewBag.Name = apartment.Name;

            return View();
        }

        public ActionResult DeleteImage(string imagePath)
        {
            string path = Server.MapPath("~/" + imagePath);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UploadImagesAsync(int id)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var httpRequest = System.Web.HttpContext.Current.Request;
            HttpFileCollection uploadFiles = httpRequest.Files;
            string fullPath = string.Empty;
            HttpPostedFile postedFile = null;

            if (httpRequest.Files.Count > 0)
            {
                postedFile = uploadFiles[0];
                string filePath = Server.MapPath("~/Images/Apartment/"+apartment.Name);
                string pic = postedFile.FileName;
                fullPath = System.IO.Path.Combine(filePath, pic);

                if (!Directory.Exists(filePath))
                {
                    DirectoryInfo di = Directory.CreateDirectory(filePath);
                }

                var versions = new Dictionary<string, string>();

                //Define the versions to generate
                versions.Add("_small", "maxwidth=600&maxheight=600&format=jpg");
                //versions.Add("_medium", "maxwidth=900&maxheight=900&format=jpg");
                //versions.Add("_large", "maxwidth=1200&maxheight=1200&format=jpg");

                //Generate each version
                foreach (var suffix in versions.Keys)
                {
                    postedFile.InputStream.Seek(0, SeekOrigin.Begin);

                    //Let the image builder add the correct extension based on the output file type
                    ImageBuilder.Current.Build(
                        new ImageJob(postedFile.InputStream, fullPath, new Instructions(versions[suffix]), false, false));
                }

                //postedFile.SaveAs(fullPath);
                //ImageCrop.CropAndOverwrite(fullPath);

            }

            return Json("/Images/Apartment/" + apartment.Name+"/" + postedFile.FileName, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UploadMainImageAsync(int id)
        {
            Apartment apartment = db.Apartment.Find(id);
            if (apartment == null)
            {
                return Json(string.Empty, JsonRequestBehavior.AllowGet);
            }

            var httpRequest = System.Web.HttpContext.Current.Request;
            HttpFileCollection uploadFiles = httpRequest.Files;
            string fullPath = string.Empty;
            HttpPostedFile postedFile = null;

            if (httpRequest.Files.Count > 0)
            {
                postedFile = uploadFiles[0];
                string filePath = Server.MapPath("~/Images/Apartment/" + apartment.Name);
                string pic = postedFile.FileName;
                fullPath = System.IO.Path.Combine(filePath, pic);

                if (!Directory.Exists(filePath))
                {
                    DirectoryInfo di = Directory.CreateDirectory(filePath);
                }

                var versions = new Dictionary<string, string>();
 
                //Define the versions to generate
                versions.Add("_small", "maxwidth=600&maxheight=600&format=jpg");
                //versions.Add("_medium", "maxwidth=900&maxheight=900&format=jpg");
                //versions.Add("_large", "maxwidth=1200&maxheight=1200&format=jpg");
 
                //Generate each version
                foreach (var suffix in versions.Keys)
                {
                    postedFile.InputStream.Seek(0, SeekOrigin.Begin);
 
                    //Let the image builder add the correct extension based on the output file type
                    ImageBuilder.Current.Build(
                        new ImageJob(postedFile.InputStream, fullPath, new Instructions(versions[suffix]), false, false));
                }

                //postedFile.SaveAs(fullPath);

                apartment.MainImagePath = fullPath;
                db.SaveChanges();

                //ImageCrop.CropAndOverwrite(fullPath);

            }

            return Json("/Images/Apartment/" + apartment.Name + "/" + postedFile.FileName, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateDates(int id)
        {
            Apartment lb_apartment = db.Apartment.Find(id);
            if (lb_apartment == null)
            {
                return HttpNotFound();
            }

            return View(lb_apartment);
        }

        [HttpPost]
        public ActionResult UpdateAvailableDates(string dat,int id,bool booked)
        {
            if (!string.IsNullOrEmpty(dat) && id > 0)
            {
                int userId = WebSecurity.GetUserId(User.Identity.Name);
                DateTime date = DateTime.Parse(dat);
                var result = db.RoomsBook.Where(r => r.ApartmentId == id && r.Date == date).FirstOrDefault();
                if (result == null)
                {
                    RoomsBook booking = new RoomsBook
                    {
                        ApartmentId = id,
                        Date = date,
                        booked = booked,
                        CreatedBy = userId,
                        CreatedDate = DateTime.Now,
                        ModifiedBy = userId,
                        ModifiedDate = DateTime.Now
                    };
                    db.RoomsBook.Add(booking);
                    db.SaveChanges();
                }
                else
                {
                    result.booked = booked;
                    result.ModifiedBy = userId;
                    result.ModifiedDate = DateTime.Now;
                    db.Entry(result).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult LoadAvailableDates(string start, string end,int id)
        {
            List<Event> events = new List<Event>();

            if (!string.IsNullOrEmpty(start) && !string.IsNullOrEmpty(end) && id > 0)
            {
                DateTime startDate = DateTime.Parse(start);
                DateTime endDate = DateTime.Parse(end);

                var result = db.RoomsBook.Where(r => r.ApartmentId == id && r.booked == true && r.Date <= endDate && r.Date >= startDate).ToList();

                foreach (var item in result)
                {
                    var date = ConvertToTimestamp(item.Date);
                    var JSONObj = new Event
                    {
                        title = "booked",
                        start = Convert.ToString(date),
                        allDay = "true",
                        end = Convert.ToString(date),
                        id = Convert.ToString(date)
                    };

                    events.Add(JSONObj);
                }
            }

            return Json(events.ToArray(), JsonRequestBehavior.AllowGet);
        }

        private double ConvertToTimestamp(DateTime value)
        {
            TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
            return (double)span.TotalSeconds;
        }

        public string getConversionRate(string CurrencyFrom, string CurrencyTo)
        {
            double rate = 1;
            try
            {
                CurrencyConvertorSoapClient curConvertor = new CurrencyConvertorSoapClient();

                rate = curConvertor.ConversionRate((Currency)Enum.Parse(typeof(Currency),
                        CurrencyFrom), (Currency)Enum.Parse(typeof(Currency), CurrencyTo));
            }
            catch (Exception)
            {

            }

            return rate.ToString();
        }

    }
}