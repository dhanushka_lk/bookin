﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace LankaBookin.Common
{
    public static class ImageCrop
    {
        public static void CropAndOverwrite(string imgPath)
        {
            Bitmap bMap = new Bitmap(imgPath);
            int size=100;

            if (bMap.Width < bMap.Height)
            {
                size = bMap.Width;
            }
            else
            {
                size = bMap.Height;
            }

            //Create a rectanagle to represent the cropping area
            Rectangle rect = new Rectangle(0, 0, size, size);
            //see if path if relative, if so set it to the full path

            //Load the original image
            
            //The format of the target image which we will use as a parameter to the Save method
            var format = bMap.RawFormat;


            //Draw the cropped part to a new Bitmap
            var croppedImage = bMap.Clone(rect, bMap.PixelFormat);

            //Dispose the original image since we don't need it any more
            bMap.Dispose();

            //Remove the original image because the Save function will throw an exception and won't Overwrite by default
            if (System.IO.File.Exists(imgPath))
                System.IO.File.Delete(imgPath);

            //Save the result in the format of the original image
            croppedImage.Save(imgPath, format);
            //Dispose the result since we saved it
            croppedImage.Dispose();
        }
    }
}