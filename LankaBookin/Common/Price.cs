﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LankaBookin.Common
{
    public class Price
    {
        public int Value { get; set; }
        public string Text { get; set; }

        public static List<Price> GetPrices = new List<Price>()
        {
            new Price() {Text="Rs 1000", Value=1000},
            new Price() { Text="Rs 2000", Value=2000},
            new Price() { Text="Rs 4000", Value=4000},
            new Price() { Text="Rs 10000", Value=10000},
            new Price() { Text="Rs 15000", Value=15000},
            new Price() { Text="Rs 20000", Value=20000},
            new Price() { Text="Rs 30000", Value=30000},
        };

    }
}