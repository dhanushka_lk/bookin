﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace LankaBookin.Common
{
    public class ErrorLog : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            var message = string.Format("Error processing URL: {0}. Exception: {1}",filterContext.HttpContext.Request.Url, filterContext.Exception.Message);
            if (filterContext.Exception.InnerException != null)
            {
                message += "; Inner exception: " + filterContext.Exception.InnerException.Message;
            }

            filterContext.HttpContext.Trace.Write(message);
            System.Diagnostics.Trace.TraceError(message);  

            string SmtpClient = System.Configuration.ConfigurationManager.AppSettings["SmtpClient"];
            string WebsiteEmail = System.Configuration.ConfigurationManager.AppSettings["WebsiteEmail"];
            string AdminEmail = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
            string Port = System.Configuration.ConfigurationManager.AppSettings["Port"];
            string Password = System.Configuration.ConfigurationManager.AppSettings["Password"];

            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient(SmtpClient);

            mail.From = new MailAddress(WebsiteEmail);
            mail.To.Add(AdminEmail);
            mail.Subject = "Error Log for Bookin";
            mail.Body = message;

            SmtpServer.Port = Convert.ToInt32(Port);
            SmtpServer.Credentials = new System.Net.NetworkCredential(WebsiteEmail, Password);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

            base.OnException(filterContext);
        }
    }
}